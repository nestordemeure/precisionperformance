import numpy as np
from pandas2numpy import Pandas2numpy

from tools.data_importation import imports_data
from tools.model import TabularModel
from tools.serialization import load_parameters
from tools.training_loop import mean_squared_error, mean_absolute_error
from tools.feature_importance import Explainer

# paths to input and training parameters
from hyperparameters import path_preprocessed_data_pkl, test_set_fraction, \
                            continuous_col_names, categorical_col_names, log_col_names, \
                            random_seed, path_residual_model_pkl, path_time_model_pkl, \
                            target_explicit_residual_column, target_time_column, \
                            path_importance_time_pdf, path_importance_residual_pdf

#--------------------------------------------------------------------------------------------------
# Data importation

# import data as dataframes
df_test, df_train = imports_data(path_preprocessed_data_pkl, test_set_fraction=test_set_fraction, random_seed=random_seed)

# defines conversion from dataframes to tensor
tabularEncoder = Pandas2numpy(df_train, continuous_columns=continuous_col_names, categorical_columns=categorical_col_names,
                              normalized_columns=continuous_col_names, logscale_columns=log_col_names, log_epsilon=1e-25)

# test dataset
x_cont, x_cat = tabularEncoder.to_numpy(df_test)
x_cat = x_cat.astype(int) # TODO while flax.nn.Embed refuses int8
example_input = [x_cont, x_cat]

# outputs
y_time = np.log(df_test[target_time_column].to_numpy())
y_residual = np.vstack(df_test[target_explicit_residual_column].to_numpy())

# keep only columns that are used as inputs
df_input = df_test[tabularEncoder.categorical_columns + tabularEncoder.continuous_columns]

#--------------------------------------------------------------------------------------------------
# Models

# defines model structure
nb_category_per_cat_col = tabularEncoder.nb_category_per_categorical_column
layer_sizes = [2000, 1000, 500, 500]
nb_outputs_time = 1
nb_outputs_residual = 2000

# loads time model following specs
model_time = TabularModel(nb_category_per_cat=nb_category_per_cat_col, layer_sizes=layer_sizes, nb_outputs=nb_outputs_time)
parameters_time = load_parameters(model_time, example_input, path_time_model_pkl)

# loads residual model following specs
model_residual = TabularModel(nb_category_per_cat=nb_category_per_cat_col, layer_sizes=layer_sizes, nb_outputs=nb_outputs_residual)
parameters_residual = load_parameters(model_residual, example_input, path_residual_model_pkl)

#--------------------------------------------------------------------------------------------------
# Feature importance time

def mean_squared_error_redim(y, y_pred):
    # get rid of empty dimension to avoid broadcast problems
    y_pred = y_pred.squeeze(axis=-1)
    return mean_squared_error(y_pred, y)

# compute importances for computing times
importances_time = Explainer(tabularEncoder, model_time, parameters_time, mean_squared_error_redim).permutation_importance(df_input, y_time, n_repeats=30)

# produces a plot as a pdf
fig_time = importances_time.plot(y='importances', kind='barh', legend=False, grid=False, figsize=(6,5)).get_figure()
fig_time.tight_layout()
fig_time.savefig(path_importance_time_pdf)

#--------------------------------------------------------------------------------------------------
# Feature importance residual

# compute importances for residual
importances_residual = Explainer(tabularEncoder, model_residual, parameters_residual, mean_absolute_error).permutation_importance(df_input, y_residual, n_repeats=30)

# produces a plot as a pdf
fig_residual = importances_residual.plot(y='importances', kind='barh', legend=False, grid=False, figsize=(6,5)).get_figure()
fig_residual.tight_layout()
fig_residual.savefig(path_importance_residual_pdf)
