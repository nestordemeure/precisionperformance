import numpy as np
from pandas2numpy import Pandas2numpy

from tools.data_importation import imports_data
from tools.model import TabularModel
from tools.serialization import load_parameters
from tools.plotting_functions import *

# paths to input and training parameters
from hyperparameters import path_preprocessed_data_pkl, test_set_fraction, \
                            continuous_col_names, categorical_col_names, log_col_names, \
                            random_seed, path_residual_model_pkl, path_time_model_pkl

#--------------------------------------------------------------------------------------------------
# Data importation

# import data as dataframes
df_test, df_train = imports_data(path_preprocessed_data_pkl, test_set_fraction=test_set_fraction, random_seed=random_seed)

# defines conversion from dataframes to tensor
tabularEncoder = Pandas2numpy(df_train, continuous_columns=continuous_col_names, categorical_columns=categorical_col_names,
                              normalized_columns=continuous_col_names, logscale_columns=log_col_names, log_epsilon=1e-25)

# test dataset
x_cont, x_cat = tabularEncoder.to_numpy(df_test)
x_cat = x_cat.astype(int) # TODO while flax.nn.Embed refuses int8
example_input = [x_cont, x_cat]

#--------------------------------------------------------------------------------------------------
# Models

# defines model structure
nb_category_per_cat_col = tabularEncoder.nb_category_per_categorical_column
layer_sizes = [2000, 1000, 500, 500]
nb_outputs_time = 1
nb_outputs_residual = 2000

# loads time model following specs
model_time = TabularModel(nb_category_per_cat=nb_category_per_cat_col, layer_sizes=layer_sizes, nb_outputs=nb_outputs_time)
parameters_time = load_parameters(model_time, example_input, path_time_model_pkl)

# loads residual model following specs
model_residual = TabularModel(nb_category_per_cat=nb_category_per_cat_col, layer_sizes=layer_sizes, nb_outputs=nb_outputs_residual)
parameters_residual = load_parameters(model_residual, example_input, path_residual_model_pkl)

#--------------------------------------------------------------------------------------------------
# Predictions

# residual
y_residual = model_residual.apply(parameters_residual, x_cont, x_cat).astype(np.float64) # type convertion to avoid inf on exp
df_test['log_residuals_predicted'] = y_residual

# time
y_time = model_time.apply(parameters_time, x_cont, x_cat).astype(np.float64) # type convertion to avoid inf on exp
df_test['time_per_iter_predicted'] = np.exp(y_time)

#--------------------------------------------------------------------------------------------------
# Plots

# residual only
#plot_single_residual(df_test)
#plot_matrix_residual(df_test)
#plot_matrix_solver_residual(df_test)
#plot_all_residual(df_test)

# time + residual
#plot_single(df_test)
plot_matrix(df_test)
#plot_matrix_solver(df_test)
#plot_all(df_test)
