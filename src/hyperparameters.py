from pathlib import Path
from tools.schedulers import constant_scheduler, knee_scheduler

#--------------------------------------------------------------------------------------------------
# PATHS

# input csv
data_folder = Path("../data")
path_residual_csv = data_folder / 'explicit.csv'
path_features_csv = data_folder / 'features.csv'
path_time_csv = data_folder / 'time.csv'
# outputs
path_preprocessed_data_pkl = data_folder / 'training_data.pkl'
path_time_model_pkl = data_folder / 'time_model.pkl'
path_residual_model_pkl = data_folder / 'residual_model.pkl'
# plots
path_importance_time_pdf = data_folder / 'importance_time.pdf'
path_importance_residual_pdf = data_folder / 'importance_residual.pdf'
path_confusionmatrix_pdf = data_folder / 'confusion_matrix.pdf'
path_minimum_residual_pdf = data_folder / 'minimum_residual.pdf'

#--------------------------------------------------------------------------------------------------
# TRAINING PARAMETERS

random_seed = 42 # to help with reproducibility
test_set_fraction = 0.2

num_epochs = 100
batch_size = 512
learning_rate = 1e-2 # 1e-3 is a better default but we will use a scheduler to decrease it
weight_decay = 1e-1
beta1 = 0.9
beta2 = 0.99
eps = 1e-8

# constant_scheduler is enough to get good results but this improves on it
explore_fraction_threshold = 0.34 # when to start decrease schedule from high initial value
learning_rate_scheduler = knee_scheduler(learning_rate, explore_fraction_threshold)
weight_decay_scheduler = knee_scheduler(weight_decay, explore_fraction_threshold)

#--------------------------------------------------------------------------------------------------
# FEATURES

# features that we want to predict
target_time_column = 'time_per_iter'
target_explicit_residual_column = 'log_residuals'

# features that should be in log space
log_col_names = ['nb_rows', 'norm_1', 'approx_lambda_min', 'approx_condition_number', 'norm_frobenius', 'approx_lambda_max2', 'approx_lambda_max', 'smallest_diagonal', 'largest_diagonal']

# all features available
all_categorical_col_names = ['solver', 'preconditionner',
                             'is_positiv_definite', 'is_symmetric', 'is_hermitian', 'is_triangular_lower', 'is_triangular_upper',
                             'sign_smallest_diagonal', 'sign_largest_diagonal']
all_continuous_col_names = ['nb_rows', 'nb_diagonal_dominant_rows', 'nb_non_zero', 'nb_non_zero_lower', 'nb_non_zero_upper', 'nb_one', 'max_non_zero_per_row',
                            'lower_bandwidth', 'upper_bandwidth', 'upperSpread', 'lowerSpread',
                            'skew_symmetric_score', 'symmetric_score', 'pattern_symmetric_score',
                            'approx_lambda_max', 'approx_lambda_max2', 'approx_lambda_min', 'approx_max_ratio', 'approx_condition_number', 'approx_peak',
                            'norm_1', 'norm_frobenius', 'largest_diagonal', 'smallest_diagonal',
                            's90', 's10', 'smid']
# useless features: 'is_triangular_lower', 'is_triangular_upper', 'sign_smallest_diagonal'
# redondant features: 'is_symmetric', 'is_hermitian', 'largest_diagonal', 'smallest_diagonal',

# features selected by bayesian optimization plus removing features of negligeable importance
categorical_col_names = ['solver', 'preconditionner', 'is_positiv_definite']
continuous_col_names = ['nb_rows', 'nb_diagonal_dominant_rows', 'nb_non_zero_lower', 'max_non_zero_per_row',
                        'lower_bandwidth', 'upper_bandwidth', 'upperSpread', 'lowerSpread',
                        'skew_symmetric_score', 'symmetric_score', 'pattern_symmetric_score',
                        'approx_lambda_max', 'approx_lambda_max2', 'approx_lambda_min',
                        'norm_1', 'norm_frobenius', 's90', 's10', 'smid']
