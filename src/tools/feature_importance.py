import pandas as pd
from sklearn.base import BaseEstimator
from sklearn.inspection import permutation_importance
from sklearn.metrics import make_scorer

class Explainer(BaseEstimator):
    def __init__(self, encoder, model, parameters, loss):
        """
        stores the information needed to compute the feature importance
        loss takes an output, an expected output and produces a number to minimize
        """
        self.encoder = encoder
        self.model = model
        self.parameters = parameters
        self.scorer = make_scorer(loss, greater_is_better=False)

    def fit(self, X, y):
        """does nothing, used to satisfy the scikitlearn API"""
        return self

    def predict(self, data):
        """takes an input and produces an output"""
        x_cont, x_cat = self.encoder.to_numpy(data) # converts dataframe into tensors
        x_cat = x_cat.astype(int) # TODO while flax.nn.Embed refuses int8
        return self.model.apply(self.parameters, x_cont, x_cat)

    def permutation_importance(self, inputs, outputs, n_repeats=5):
        """computes the feature importance using the given inputs and outputs"""
        # computes feature importance by the permutation method
        print("Computing feature importance, this might take some time...")
        importances = permutation_importance(self, inputs, outputs, scoring=self.scorer, n_repeats=n_repeats)
        importances = pd.DataFrame(importances, index=inputs.columns)
        print("Done.")
        # take absolute value of importances
        importances['importances'] = importances['importances_mean'].abs()
        # sort importances
        importances = importances.sort_values('importances')
        return importances
