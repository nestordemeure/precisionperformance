import pandas as pd
import numpy as np

def imports_data(pkl_dataframe_path, test_set_fraction=0.2, random_seed=42):
    """
    imports a dataset as a pair (df_test,df_train) with the validation and training data stored in dataframes
    df_test contains `test_set_fraction` of the matrices available in the dataset
    uses a fixed seed, `random_seed`, to ensure reproducibility
    """
    # to insure that we have always the same test set
    np.random.seed(random_seed)
    # loads the data
    dataframe = pd.read_pickle(pkl_dataframe_path)
    # use a fraction of matrices as test data
    all_matrix_names = dataframe['matrix'].unique()
    nb_test_matrices = int(len(all_matrix_names) * test_set_fraction)
    name_test_matrices = np.random.choice(all_matrix_names, size=nb_test_matrices, replace=False)
    # gets the indexes of the validation rows
    #indexes_validation_set = dataframe[dataframe['matrix'].isin(name_test_matrices)].index.tolist()
    # extracts test and train sets as different dataframes
    df_test = dataframe[dataframe['matrix'].isin(name_test_matrices)]
    df_train = dataframe[~dataframe['matrix'].isin(name_test_matrices)]
    return (df_test, df_train)
