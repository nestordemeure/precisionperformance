import time
import matplotlib.pyplot as plt

class TrainingStats():
    "Use to store training data."
    def __init__(self):
        # collects statistics for display
        self.test_measure_iterations = []
        self.test_losses = []
        self.train_losses = []
        self.epoch_start = None
        self.epoch = 0
        self.best_test_loss = None
        self.best_model = None

    def add_train_loss(self, value):
        "adds one train loss, we expect that this function will be called for all train losses"
        self.train_losses.append(value)

    def add_test_loss(self, value, model=None):
        "adds one test loss, we expect that this function will be called after adding the corresponding train loss"
        iteration = self.iteration()
        self.test_measure_iterations.append(iteration)
        self.test_losses.append(value)
        if (self.best_test_loss is None) or (value < self.best_test_loss):
            self.best_test_loss = value
            self.best_model = self.best_model if model is None else model

    def start_epoch(self):
        "sets a timer to measure the time spent in computations"
        self.epoch_start = time.time()
        self.epoch += 1

    def get_epoch_time(self):
        "gets back the result form the timer"
        if self.epoch_start is None: raise AssertionError("You need to call `start_epoch` at the begining of the epoch")
        epoch_time = time.time() - self.epoch_start
        self.epoch_start = None
        return epoch_time

    def end_epoch(self, test_loss=None, model=None):
        "displays some informations on the epoch and stores the test loss"
        if not (test_loss is None): self.add_test_loss(test_loss, model=model)
        epoch_time = self.get_epoch_time()
        test_loss = self.test_losses[-1]
        train_loss = self.train_losses[-1]
        print(f'Epoch {self.epoch} in {epoch_time:0.2f}s. Test loss: {test_loss:e} Train loss: {train_loss:e}')

    def iteration(self):
        "number of iterations so far"
        return len(self.train_losses) - 1

    def plot_training_losses(self, useLog=True):
        "displays the losses collected so far"
        plt.plot(self.train_losses, label='train loss')
        plt.plot(self.test_measure_iterations, self.test_losses, label='test loss')
        if useLog: plt.yscale('log')
        plt.ylabel('Loss')
        plt.xlabel('Iteration')
        plt.legend()
        plt.show()
