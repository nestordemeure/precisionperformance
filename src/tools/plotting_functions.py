import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go # interactive plots
import plotly.express as px # plot colors

#--------------------------------------------------------------------------------------------------
# Residual

def plot_single_residual(df):
    "plot a single, random, residual as a function of the number of iterations"
    # get random test case (within the test data)
    row = df.sample(1).iloc[0]
    # gets target and prediction
    target = np.exp(row['log_residuals'])
    prediction = np.exp(row['log_residuals_predicted'])
    # plots curves
    plt.style.use('dark_background')
    plt.figure(figsize=(10,10))
    plt.title(row['matrix'] + ' (' + row['solver'] + ' ' + row['preconditionner'] + ')')
    plt.plot(target, label="Explicit residual")
    plt.plot(prediction, label="Predicted explicit residual")
    plt.yscale('log')
    plt.ylabel('Relative residual')
    plt.xlabel('Iteration')
    plt.legend(loc="upper right")
    plt.show()

def plot_matrix_residual(df):
    "plots a single matrix and all solvers with, for each, a random preconditionneur"
    # choose a random matrix
    plotted_matrix = df['matrix'].sample(1).iloc[0]
    plotted_df = df[df['matrix'] == plotted_matrix]
    # prepare plot
    fig = go.Figure()
    fig.update_layout(title=plotted_matrix, xaxis_title='iteration', yaxis_title='explicit residual', showlegend=False, yaxis_type="log", width=700, height=500, template="plotly_dark", yaxis = dict(showexponent = 'all', exponentformat = 'e'))
    colors = px.colors.qualitative.Light24_r
    plot_id = 0
    # iterate on all solvers
    for plotted_solver in plotted_df['solver'].unique():
        plotted_df_solver = plotted_df[plotted_df['solver'] == plotted_solver]
        # take a randopm preconditionneur
        precond = plotted_df_solver['preconditionner'].sample(1).iloc[0]
        row = plotted_df_solver[plotted_df_solver['preconditionner'] == precond].iloc[0]
        # gets target and prediction
        target = np.exp(row['log_residuals'])
        prediction = np.exp(row['log_residuals_predicted'])
        # plots curves
        plot_color = colors[plot_id % len(colors)]
        fig.add_trace(go.Scatter(y=target, mode='lines', text=plotted_solver + ' | ' + precond+ ' (target)', hoverinfo="text", line=dict(color=plot_color)))
        fig.add_trace(go.Scatter(y=prediction, mode='lines', text=plotted_solver + ' | ' + precond + ' (predicted)', hoverinfo="text", line=dict(color=plot_color, dash='dot')))
        plot_id += 1
    fig.show()

def plot_matrix_solver_residual(df):
    "plots a single matrix, a solver and all preconditionneurs"
    # choose a random matrix
    plotted_matrix = df['matrix'].sample(1).iloc[0]
    plotted_df = df[df['matrix'] == plotted_matrix]
    # choose a random solver
    plotted_solver = plotted_df['solver'].sample(1).iloc[0]
    plotted_df = plotted_df[plotted_df['solver'] == plotted_solver]
    # prepare plot
    fig = go.Figure()
    fig.update_layout(title=plotted_matrix + ' (' + plotted_solver + ')', xaxis_title='iteration', yaxis_title='explicit residual', showlegend=False, yaxis_type="log", width=700, height=500, template="plotly_dark", yaxis = dict(showexponent = 'all', exponentformat = 'e'))
    colors = px.colors.qualitative.Light24_r
    plot_id = 0
    # iterate on all preconditionners
    for precond in plotted_df['preconditionner'].unique():
        row = plotted_df[plotted_df['preconditionner'] == precond].iloc[0]
        # gets target and prediction
        target = np.exp(row['log_residuals'])
        prediction = np.exp(row['log_residuals_predicted'])
        # plots curves
        plot_color = colors[plot_id % len(colors)]
        fig.add_trace(go.Scatter(y=target, mode='lines', text=precond+ ' (target)', hoverinfo="text", line=dict(color=plot_color)))
        fig.add_trace(go.Scatter(y=prediction, mode='lines', text=precond + ' (predicted)', hoverinfo="text", line=dict(color=plot_color, dash='dot')))
        plot_id += 1
    fig.show()

def plot_all_residual(df):
    "plots all solver*precond combinaisons for a matrix"
    # choose a random matrix
    plotted_matrix = df['matrix'].sample(1).iloc[0]
    plotted_df = df[df['matrix'] == plotted_matrix]
    # prepare plot
    fig = go.Figure()
    fig.update_layout(title=plotted_matrix, xaxis_title='iteration', yaxis_title='explicit residual', showlegend=False, yaxis_type="log", width=700, height=500, template="plotly_dark", yaxis = dict(showexponent = 'all', exponentformat = 'e'))
    colors = px.colors.qualitative.Light24_r
    plot_id = 0
    # iterate on all solvers
    for solver in plotted_df['solver'].unique():
        plotted_df_solver = plotted_df[plotted_df['solver'] == solver]
        # iterate on all preconditionners
        for precond in plotted_df_solver['preconditionner'].unique():
            row = plotted_df_solver[plotted_df_solver['preconditionner'] == precond].iloc[0]
            # gets target and prediction
            target = np.exp(row['log_residuals'])
            prediction = np.exp(row['log_residuals_predicted'])
            # plots curves
            plot_color = colors[plot_id % len(colors)]
            fig.add_trace(go.Scatter(y=target, mode='lines', text=solver + ' | ' + precond+ ' (target)', hoverinfo="text", line=dict(color=plot_color)))
            fig.add_trace(go.Scatter(y=prediction, mode='lines', text=solver + ' | ' + precond + ' (predicted)', hoverinfo="text", line=dict(color=plot_color, dash='dot')))
            plot_id += 1
    fig.show()

#--------------------------------------------------------------------------------------------------
# Time profile

def plot_single(df):
    "plot a single, random, residual as a function of the number of time"
    # get random test case (within the test data)
    row = df.sample(1).iloc[0]
    # gets target and prediction
    target_x = range(2000) * row['time_per_iter']
    target_y = np.exp(row['log_residuals'])
    prediction_x = range(2000) * row['time_per_iter_predicted']
    prediction_y = np.exp(row['log_residuals_predicted'])
    # plots curves
    plt.style.use('dark_background')
    plt.figure(figsize=(10,10))
    plt.title(row['matrix'] + ' (' + row['solver'] + ' ' + row['preconditionner'] + ')')
    plt.plot(target_x, target_y, label="Explicit residual")
    plt.plot(prediction_x, prediction_y, label="Predicted explicit residual")
    plt.yscale('log')
    plt.ylabel('Relative residual')
    plt.xlabel('Seconds')
    plt.legend(loc="upper right")
    plt.show()

def plot_matrix(df):
    "plots a single matrix and all solvers with, for each, a random preconditionneur"
    # choose a random matrix
    plotted_matrix = df['matrix'].sample(1).iloc[0]
    plotted_df = df[df['matrix'] == plotted_matrix]
    # prepare plot
    fig = go.Figure()
    fig.update_layout(title=plotted_matrix, xaxis_title='iteration', yaxis_title='explicit residual', showlegend=False, yaxis_type="log", width=700, height=500, template="plotly_dark", yaxis = dict(showexponent = 'all', exponentformat = 'e'))
    colors = px.colors.qualitative.Light24_r
    plot_id = 0
    # iterate on all solvers
    for plotted_solver in plotted_df['solver'].unique():
        plotted_df_solver = plotted_df[plotted_df['solver'] == plotted_solver]
        # take a randopm preconditionneur
        precond = plotted_df_solver['preconditionner'].sample(1).iloc[0]
        row = plotted_df_solver[plotted_df_solver['preconditionner'] == precond].iloc[0]
        # gets target and prediction
        target_x = range(2000) * row['time_per_iter']
        target_y = np.exp(row['log_residuals'])
        prediction_x = range(2000) * row['time_per_iter_predicted']
        prediction_y = np.exp(row['log_residuals_predicted'])
        # plots curves
        plot_color = colors[plot_id % len(colors)]
        fig.add_trace(go.Scatter(x=target_x, y=target_y, mode='lines', text=plotted_solver + ' | ' + precond+ ' (target)', hoverinfo="text", line=dict(color=plot_color)))
        fig.add_trace(go.Scatter(x=prediction_x, y=prediction_y, mode='lines', text=plotted_solver + ' | ' + precond + ' (predicted)', hoverinfo="text", line=dict(color=plot_color, dash='dot')))
        plot_id += 1
    fig.show()

def plot_matrix_solver(df):
    "plots a single matrix, a solver and all preconditionneurs"
    # choose a random matrix
    plotted_matrix = df['matrix'].sample(1).iloc[0]
    plotted_df = df[df['matrix'] == plotted_matrix]
    # choose a random solver
    plotted_solver = plotted_df['solver'].sample(1).iloc[0]
    plotted_df = plotted_df[plotted_df['solver'] == plotted_solver]
    # prepare plot
    fig = go.Figure()
    fig.update_layout(title=plotted_matrix + ' (' + plotted_solver + ')', xaxis_title='iteration', yaxis_title='explicit residual', showlegend=False, yaxis_type="log", width=700, height=500, template="plotly_dark", yaxis = dict(showexponent = 'all', exponentformat = 'e'))
    colors = px.colors.qualitative.Light24_r
    plot_id = 0
    # iterate on all preconditionners
    for precond in plotted_df['preconditionner'].unique():
        row = plotted_df[plotted_df['preconditionner'] == precond].iloc[0]
        # gets target and prediction
        target_x = range(2000) * row['time_per_iter']
        target_y = np.exp(row['log_residuals'])
        prediction_x = range(2000) * row['time_per_iter_predicted']
        prediction_y = np.exp(row['log_residuals_predicted'])
        # plots curves
        plot_color = colors[plot_id % len(colors)]
        fig.add_trace(go.Scatter(x=target_x, y=target_y, mode='lines', text=precond+ ' (target)', hoverinfo="text", line=dict(color=plot_color)))
        fig.add_trace(go.Scatter(x=prediction_x, y=prediction_y, mode='lines', text=precond + ' (predicted)', hoverinfo="text", line=dict(color=plot_color, dash='dot')))
        plot_id += 1
    fig.show()

def plot_all(df):
    "plots all solver*precond combinaisons for a matrix"
    # choose a random matrix
    plotted_matrix = df['matrix'].sample(1).iloc[0]
    plotted_df = df[df['matrix'] == plotted_matrix]
    # prepare plot
    fig = go.Figure()
    fig.update_layout(title=plotted_matrix, xaxis_title='iteration', yaxis_title='explicit residual', showlegend=False, yaxis_type="log", width=700, height=500, template="plotly_dark", yaxis = dict(showexponent = 'all', exponentformat = 'e'))
    colors = px.colors.qualitative.Light24_r
    plot_id = 0
    # iterate on all solvers
    for solver in plotted_df['solver'].unique():
        plotted_df_solver = plotted_df[plotted_df['solver'] == solver]
        # iterate on all preconditionners
        for precond in plotted_df_solver['preconditionner'].unique():
            row = plotted_df_solver[plotted_df_solver['preconditionner'] == precond].iloc[0]
            # gets target and prediction
            target_x = range(2000) * row['time_per_iter']
            target_y = np.exp(row['log_residuals'])
            prediction_x = range(2000) * row['time_per_iter_predicted']
            prediction_y = np.exp(row['log_residuals_predicted'])
            # plots curves
            plot_color = colors[plot_id % len(colors)]
            fig.add_trace(go.Scatter(x=target_x, y=target_y, mode='lines', text=solver + ' | ' + precond+ ' (target)', hoverinfo="text", line=dict(color=plot_color)))
            fig.add_trace(go.Scatter(x=prediction_x, y=prediction_y, mode='lines', text=solver + ' | ' + precond + ' (predicted)', hoverinfo="text", line=dict(color=plot_color, dash='dot')))
            plot_id += 1
    fig.show()
