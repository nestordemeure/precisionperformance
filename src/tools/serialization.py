import pickle
from flax import serialization

from .training_loop import initialize_parameters

def save_parameters(parameters, path):
    "save parameters to disk"
    states_dict = serialization.to_state_dict(parameters)
    with open(path, 'wb') as file:
        pickle.dump(obj=states_dict, file=file)

def load_parameters(model, input_example, path):
    "load parameters given the correxponding model and an example of input"
    with open(path, 'rb') as file:
        states_dict = pickle.load(file=file)
    parameters_structure = initialize_parameters(model, input_example)
    parameters = serialization.from_state_dict(parameters_structure, states_dict)
    return parameters
