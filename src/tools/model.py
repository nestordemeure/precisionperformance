from typing import List
from flax.linen import compact, Module, Dense, Embed, softplus
import jax.numpy as jnp

def mish(x):
    """
    Mish activation function, using it instead of relu makes the model a little slower but better.
    """
    return x * jnp.tanh(softplus(x))

def get_embedding_size(nb_categories):
    """
    Rule of thumb to pick embedding size corresponding to `nb_categories`
    Inspired by [Fastai](https://github.com/fastai/fastai/blob/master/fastai/tabular/model.py#L10)
    """
    return int(min(600, round(1.6 * nb_categories**0.56)))

class MultiEmbeddings(Module):
    """Embeddings for multiple categorical columns with different number of category and target embedding sizes"""
    nb_category_per_cat: List[int]

    @compact
    def __call__(self, x_categorial):
        embeddings = []
        for i,category_number in enumerate(self.nb_category_per_cat):
            x = x_categorial[:,i]
            embedding_size = get_embedding_size(category_number)
            x = Embed(num_embeddings=category_number, features=embedding_size)(x)
            embeddings.append(x)
        return jnp.concatenate(embeddings, axis=1)

class TabularModel(Module):
    """
    Basic model for tabular data.
    Simple fully connected network on top of embeddings + continuous features
    """
    nb_category_per_cat: List[int]
    layer_sizes: List[int]
    nb_outputs: int

    @compact
    def __call__(self, x_cont, x_cat):
        # prepare inputs
        x_cat = MultiEmbeddings(self.nb_category_per_cat)(x_cat)
        x = jnp.concatenate([x_cont, x_cat], axis=1)
        # applies inner layers
        for output_size in self.layer_sizes:
            x = Dense(features=output_size)(x)
            x = mish(x)
        # applies final layer
        x = Dense(features=self.nb_outputs)(x)
        return x
