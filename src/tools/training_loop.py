from batchup import data_source
import numpy as np
import jax
from flax.optim import OptimizerDef

from .training_statistics import TrainingStats
from .schedulers import compute_nb_iteration

#--------------------------------------------------------------------------------------------------
# LOSS FUNCTIONS

def mean_squared_error(predictions, targets):
    return jax.numpy.square(predictions - targets).mean()

def mean_absolute_error(predictions, targets):
    return jax.numpy.abs(predictions - targets).mean()

#--------------------------------------------------------------------------------------------------
# TRAINING LOOP

def initialize_parameters(model, input_example, random_seed=42):
    """
    takes a FLAX model, an example input tensor and a random seed (to insure reproducibility)
    produces initial parameters for the model
    """
    rng = jax.random.PRNGKey(random_seed)
    parameters = model.init(rng, *input_example)
    return parameters

def training_loop(parameters, loss_function, optimizer,
                  train_dataset, test_dataset, batch_size, num_epochs,
                  learning_rate_scheduler, weight_decay_scheduler,
                  random_seed=42):
    """
    `parameters` is the initial parameters for the model that needs to be optimized
    `loss_function` takes parameters, a batch and returns the loss
    `optimizer` is a FLAX optimizer description
    `train_dataset` and `test_dataset` are lists of tensor with train and test data (the last element of each list being the expected output)
    `batch_size` is the size of the individual batches
    `num_epochs` is the number of epochs for which the optimizer will run
    `learning_rate_scheduler` and `weight_decay_scheduler` are functions that take the current iteration, the number of iterations and return a learning_rate or weight decay
    `random_seed` is a seed used for reproducibility
    """
    # initialization
    if isinstance(optimizer, OptimizerDef): optimizer = optimizer.create(parameters)
    np.random.seed(random_seed) # insures reproducible batch order
    max_iteration = compute_nb_iteration(train_dataset, batch_size, num_epochs)
    # jitted loss function
    @jax.jit
    def train_step(optimizer, batch, learning_rate, weight_decay):
        def loss_fn(parameters): return loss_function(parameters, batch)
        loss_value, loss_grad = jax.value_and_grad(loss_fn)(optimizer.target)
        optimizer = optimizer.apply_gradient(loss_grad, learning_rate=learning_rate, weight_decay=weight_decay)
        return optimizer, loss_value
    loss_jitted = jax.jit(loss_function)
    # training loop
    print("Starting training...")
    batch_source = data_source.ArrayDataSource(train_dataset)
    trainingStatistics = TrainingStats()
    for epoch in range(num_epochs):
        trainingStatistics.start_epoch()
        # iterates on all batches
        batch_iterator = batch_source.batch_iterator(batch_size=batch_size, shuffle=True)
        for batch in batch_iterator:
            iteration = trainingStatistics.iteration()
            learning_rate = learning_rate_scheduler(iteration, max_iteration)
            weight_decay = weight_decay_scheduler(iteration, max_iteration)
            optimizer, loss_value = train_step(optimizer, batch, learning_rate, weight_decay)
            trainingStatistics.add_train_loss(loss_value)
        # measure validation error
        test_loss = loss_jitted(optimizer.target, test_dataset)
        trainingStatistics.end_epoch(test_loss, model=optimizer.target)
    print(f"Training done (final test accuracy: {trainingStatistics.best_test_loss:e}).")
    trainingStatistics.plot_training_losses()
    return trainingStatistics.best_model
