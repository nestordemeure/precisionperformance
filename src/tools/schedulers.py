import math

#--------------------------------------------------------------------------------------------------
# Utility functions

def compute_nb_iteration(train_dataset, batch_size, num_epochs):
    """computes the total number of iterations that will be run"""
    nb_rows = train_dataset[0].shape[0]
    batch_per_epoch = int(math.ceil(nb_rows / batch_size))
    nb_iterations = num_epochs * batch_per_epoch
    return nb_iterations

def compute_iteration_fraction(iteration, max_iteration):
    """computes the fraction of iterations already spent"""
    return float(iteration) / float(max_iteration)

#--------------------------------------------------------------------------------------------------
# Schedulers

def constant_scheduler(value):
    """a scheduler that stays constant"""
    def scheduler(iteration, max_iteration): return value
    return scheduler

def knee_scheduler(value, explore_fraction_threshold=0.34):
    """
    a scheduler that stays constant until `explore_fraction_threshold` and then decrease to zero linearly
    see [Wide-minima Density Hypothesis and the Explore-Exploit Learning Rate Schedule](https://arxiv.org/abs/2003.03977)
    """
    def scheduler(iteration, max_iteration):
        iter_fraction = compute_iteration_fraction(iteration, max_iteration)
        if iter_fraction < explore_fraction_threshold: return value
        linear_decrease_scaling = (1.0 - iter_fraction) / (1.0  - explore_fraction_threshold)
        return value * linear_decrease_scaling
    return scheduler
