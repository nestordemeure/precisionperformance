from collections import defaultdict
import csv
import numpy as np
import pandas as pd

# paths to inputs and output
from hyperparameters import path_residual_csv, path_time_csv, path_features_csv, path_preprocessed_data_pkl

#------------------------------------------------------------------------------
# RESIDUAL

def format_residuals(residuals, expected_nb_residuals=2000):
    """
    converts residuals into floats
    insures that the residual is of the expected lenght
    that there are no infinite in the residual
    that we work with the logarithms of all values
    """
    # converts string to float, takes the logarithm and stores the result unless it is infinite
    for i,r in enumerate(residuals):
        r = np.log(float(r))
        if np.isinf(r): residuals[i] = residuals[i-1]
        else: residuals[i] = r
    # insures that there is a given number of residuals
    if len(residuals) > expected_nb_residuals: raise Exception("read_residual_csv: 'expected_nb_residuals' seem to be too low!")
    if len(residuals) < expected_nb_residuals:
        last_element = residuals[-1]
        nb_additional_elements = expected_nb_residuals - len(residuals)
        residuals = residuals + [last_element] * nb_additional_elements
    return np.array(residuals)

# TODO this is slow
# paralelism might help, see:
# https://stackoverflow.com/a/51968275/6422174
def read_residual_csv(path, expected_nb_residuals=2000):
    "takes a residual csv (with a variable number fo columns) and parses it properly"
    with open(path, newline='') as file:
        reader = csv.reader(file, delimiter='\t')
        data = defaultdict(list)
        for row in reader:
            if len(row) > 5: # if there is at least one recorded residual
                data['matrix'].append(row[0])
                data['solver'].append(row[1])
                data['preconditionner'].append(row[2] + '_' + row[3])
                data['log_residuals'].append(format_residuals(row[4:-1], expected_nb_residuals))
        return pd.DataFrame(data, dtype="object")

# Imports CSV such that the residual are stored as a numpy array in a column.
df_residual = read_residual_csv(path_residual_csv, expected_nb_residuals=2000)

# converts some columns to categorial datatypes
df_residual['matrix'] = df_residual['matrix'].astype('category')
df_residual['solver'] = df_residual['solver'].astype('category')
df_residual['preconditionner'] = df_residual['preconditionner'].astype('category')

#------------------------------------------------------------------------------
# COMPUTING TIME

# computing time per matrix*solver*preconditionner*precondSide
df_time = pd.read_csv(path_time_csv, sep='\t')

df_time['preconditionner'] = df_time['preconditionner'] + '_' + df_time['precondSide'] # fuse preconditioner and side as a single value
df_time['time_per_iter'] = df_time['time'] / df_time['iterations'] # stores time per iteration metric
df_time.drop(columns=['precondSide', 'status', 'residual', 'iterations', 'time'], inplace=True)

# converts some columns to categorial datatypes
df_time['matrix'] = df_time['matrix'].astype('category')
df_time['solver'] = df_time['solver'].astype('category')
df_time['preconditionner'] = df_time['preconditionner'].astype('category')

#------------------------------------------------------------------------------
# MATRIX FEATURES

# features per matrix
df_features = pd.read_csv(path_features_csv, sep='\t', na_values='missing')

# converts string into categories to reduce memory usage
df_features['matrix'] = df_features['matrix'].astype('category')

# normalize features, dividing by the number of rows or cells
size_feature = 'nb_rows'
linear_features = ['max_non_zero_per_row', 'nb_diagonal_dominant_rows', 'lower_bandwidth', 'upper_bandwidth']
quadratic_features = ['nb_non_zero', 'nb_non_zero_lower', 'nb_non_zero_upper', 'nb_one']
for feature in linear_features: df_features[feature] /= df_features[size_feature]
for feature in quadratic_features: df_features[feature] /= df_features[size_feature].pow(2)

#------------------------------------------------------------------------------
# MERGING AND EXPORTATION

# merges the three dataframes
df = pd.merge(df_residual, df_time, on=['matrix', 'solver', 'preconditionner'], copy=False)
df = pd.merge(df, df_features, on='matrix', copy=False)

# exports the resulting dataframe as a pickle object for fast loading
df.to_pickle(path_preprocessed_data_pkl)
