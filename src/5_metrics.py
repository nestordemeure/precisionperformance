import numpy as np
#import pandas as pd
from scikitplot.metrics import plot_confusion_matrix
from pandas2numpy import Pandas2numpy

from tools.data_importation import imports_data
from tools.model import TabularModel
from tools.serialization import load_parameters

# paths to input and training parameters
from hyperparameters import path_preprocessed_data_pkl, test_set_fraction, \
                            continuous_col_names, categorical_col_names, log_col_names, \
                            random_seed, path_residual_model_pkl, path_time_model_pkl, \
                            path_confusionmatrix_pdf, path_minimum_residual_pdf

#--------------------------------------------------------------------------------------------------
# Data importation

# import data as dataframes
df_test, df_train = imports_data(path_preprocessed_data_pkl, test_set_fraction=test_set_fraction, random_seed=random_seed)

# defines conversion from dataframes to tensor
tabularEncoder = Pandas2numpy(df_train, continuous_columns=continuous_col_names, categorical_columns=categorical_col_names,
                              normalized_columns=continuous_col_names, logscale_columns=log_col_names, log_epsilon=1e-25)

# test dataset
x_cont, x_cat = tabularEncoder.to_numpy(df_test)
x_cat = x_cat.astype(int) # TODO while flax.nn.Embed refuses int8
example_input = [x_cont, x_cat]

#--------------------------------------------------------------------------------------------------
# Models

# defines model structure
nb_category_per_cat_col = tabularEncoder.nb_category_per_categorical_column
layer_sizes = [2000, 1000, 500, 500]
nb_outputs_time = 1
nb_outputs_residual = 2000

# loads time model following specs
model_time = TabularModel(nb_category_per_cat=nb_category_per_cat_col, layer_sizes=layer_sizes, nb_outputs=nb_outputs_time)
parameters_time = load_parameters(model_time, example_input, path_time_model_pkl)

# loads residual model following specs
model_residual = TabularModel(nb_category_per_cat=nb_category_per_cat_col, layer_sizes=layer_sizes, nb_outputs=nb_outputs_residual)
parameters_residual = load_parameters(model_residual, example_input, path_residual_model_pkl)

#--------------------------------------------------------------------------------------------------
# Predictions

# residual
y_residual = model_residual.apply(parameters_residual, x_cont, x_cat).astype(np.float64) # type convertion to avoid inf on exp
df_test['log_residuals_predicted'] = y_residual

# time
y_time = model_time.apply(parameters_time, x_cont, x_cat).astype(np.float64) # type convertion to avoid inf on exp
df_test['time_per_iter_predicted'] = np.exp(y_time)

#--------------------------------------------------------------------------------------------------
# Residual prediction metrics

def get_min_residual(log_residuals):
    """computes the minimum residual"""
    return np.exp(np.min(log_residuals))

def class_of_residual(min_residual):
    """classify the minimum reached residual in 4 coarse categories"""
    if min_residual > 1.: return "above 1."
    if min_residual > 1e-6: return "[1, 1e-6]"
    if min_residual > 1e-10: return "[1e-6, 1e-10]"
    return "below 1e-10"

# computes the actual and predicted minimum residual
df_test['predicted_min_residual'] = df_test['log_residuals_predicted'].apply(get_min_residual)
df_test['min_residual'] = df_test['log_residuals'].apply(get_min_residual)
# compute the actual class and the predicted class
df_test['predicted_class'] = df_test['predicted_min_residual'].apply(class_of_residual)
df_test['class'] = df_test['min_residual'].apply(class_of_residual)

# plots minimum residual predicted and actual
# for understanding purposes
# we can see that the model does not modelize the fact that float put a lower bound on the residual and that it underestimates some divergences
# the lower bound might be fixable but we would lose potential ranking information
fig_minres = df_test.plot(x='predicted_min_residual', y='min_residual', kind='scatter', color='blue', logx=True, logy=True).get_figure()
fig_minres.tight_layout()
fig_minres.savefig(path_minimum_residual_pdf)

# https://scikit-plot.readthedocs.io/en/stable/metrics.html
# plots confusion matrix to see how well we manage to classify elements relative to the thresholds
fig_conf = plot_confusion_matrix(y_true=df_test['class'], y_pred=df_test['predicted_class'],
                                 labels=["above 1.", "[1, 1e-6]", "[1e-6, 1e-10]", "below 1e-10"], title=" ",
                                 normalize=False, figsize=(8,6), text_fontsize='large').get_figure()
fig_conf.tight_layout()
fig_conf.savefig(path_confusionmatrix_pdf)

# by which factor are we off in average
# 16.96074253762219 (about one order of magnitude)
# 0.8322676021916586 without abs
geometric_mean = np.exp(np.mean(np.abs(np.log(df_test['predicted_min_residual']) - np.log(df_test['min_residual']))))
print("Off by a factor", geometric_mean, "in average (geometric mean)")

solvers_hard_class = np.mean(df_test['class'] == "[1e-6, 1e-10]")
print("There are", solvers_hard_class*100, "percents of solvers in the [1e-6, 1e-10] class.")

print("Minimum observed relative residual", np.min(df_test['min_residual']))

#--------------------------------------------------------------------------------------------------
# iter before threhold

def iter_before_threshold(log_residuals, threshold):
    """
    number of iterations before we get below a threshold
    we suppose it IS reachable in the given array
    """
    return np.argmax(log_residuals < np.log(threshold))

# for all thresholds, finds our average error in predicting when they will be met
for t in [1.0, 1e-2, 1e-4, 1e-6, 1e-8, 1e-10]:
    # selects subset of rows where both the prediction and the actual computation reach a value
    df_t = df_test[(df_test['min_residual'] < t) & (df_test['predicted_min_residual'] < t)]
    # count number of iteration to reach that value
    iter_to_t = df_t['log_residuals'].apply(lambda r: iter_before_threshold(r, t))
    iter_to_t_predicted = df_t['log_residuals_predicted'].apply(lambda r: iter_before_threshold(r, t))
    # average absolute error
    error = np.mean(np.abs(iter_to_t - iter_to_t_predicted))
    print("average error in number of iterations to reach", t, ":", error)
    # average shift
    shift = np.mean(iter_to_t - iter_to_t_predicted)
    print("average shift in number of iterations to reach", t, ":", shift)
