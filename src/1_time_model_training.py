import numpy as np
from flax import optim
from pandas2numpy import Pandas2numpy

from tools.data_importation import imports_data
from tools.model import TabularModel
from tools.training_loop import initialize_parameters, training_loop, mean_squared_error
from tools.serialization import save_parameters

# paths to input and training parameters
from hyperparameters import path_preprocessed_data_pkl, test_set_fraction, random_seed, \
                            target_time_column, continuous_col_names, categorical_col_names, log_col_names, \
                            num_epochs, batch_size, learning_rate, weight_decay, beta1, beta2, eps, \
                            learning_rate_scheduler, weight_decay_scheduler, \
                            path_time_model_pkl \

#--------------------------------------------------------------------------------------------------
# Data importation

# import data as dataframes
df_test, df_train = imports_data(path_preprocessed_data_pkl, test_set_fraction=test_set_fraction, random_seed=random_seed)

# defines conversion from dataframes to tensor
tabularEncoder = Pandas2numpy(df_train, continuous_columns=continuous_col_names, categorical_columns=categorical_col_names,
                              normalized_columns=continuous_col_names, logscale_columns=log_col_names, log_epsilon=1e-25)

# training dataset
train_x_cont, train_x_cat = tabularEncoder.to_numpy(df_train)
train_x_cat = train_x_cat.astype(int) # TODO while flax.nn.Embed refuses int8
train_y = np.log(df_train[target_time_column].to_numpy()) # in log scale
train_dataset = [train_x_cont, train_x_cat, train_y]

# testing dataset
test_x_cont, test_x_cat = tabularEncoder.to_numpy(df_test)
test_x_cat = test_x_cat.astype(int) # TODO while flax.nn.Embed refuses int8
test_y = np.log(df_test[target_time_column].to_numpy())
test_dataset = [test_x_cont, test_x_cat, test_y]

#--------------------------------------------------------------------------------------------------
# Model

# defines model structure
nb_category_per_cat_col = tabularEncoder.nb_category_per_categorical_column
layer_sizes = [2000, 1000, 500, 500]
nb_outputs = 1

# builds model according to specs
model = TabularModel(nb_category_per_cat=nb_category_per_cat_col, layer_sizes=layer_sizes, nb_outputs=nb_outputs)

#--------------------------------------------------------------------------------------------------
# Training

# function that we will try to optimize
def loss_function(parameters, batch):
    x_cont, x_cat, y = batch
    y_pred = model.apply(parameters, x_cont, x_cat)
    # get rid of empty dimension to avoid broadcast problems
    y_pred = y_pred.squeeze(axis=-1)
    return mean_squared_error(y_pred, y)

#num_epochs=20
optimizer = optim.Adam(learning_rate=learning_rate, weight_decay=weight_decay, beta1=beta1, beta2=beta2, eps=eps)
parameters = initialize_parameters(model, input_example=(test_x_cont, test_x_cat), random_seed=random_seed)
parameters = training_loop(parameters, loss_function, optimizer, train_dataset, test_dataset, batch_size, num_epochs, 
                           learning_rate_scheduler, weight_decay_scheduler, random_seed)

save_parameters(parameters, path_time_model_pkl)
