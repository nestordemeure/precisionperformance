# Residual profile prediction code

A model that tries to predicts the residual profile of a linear solver for a given problem.

## Code structure

The code is structured as several scripts meant to be run one after the other.

#### `0_data_preprocessing.py`
The code to turn raw csv into Pandas dataframe that are the serialized for fast loading.
This code ensures that the residual have the proper length, and normalize the inputs, dividing by the matrix size.

#### `1_time_model_training.py`
The code to train the model that takes features and predicts the logarithm of the mean runtime.

#### `2_residual_model_training.py`
The code to train the model that takes features and predicts the logarithm of the residual plot (as a funciton of the number of iterations).

#### `3_plot_combined_model.py`
The code to run the trained models on the test data, stores the results and displays random matrices.

#### `4_feature_importance.py`
The code to produce the feature importance plots.

#### `5_metrics.py`
The code to produce the confusion matrix and metrics from the result section of the paper.

## Dependencies

#### Data preparation:
- [Numpy](https://numpy.org/)
- [Pandas](https://pandas.pydata.org/)
- [Batchup](https://github.com/Britefury/batchup)
- [Pandas2Numpy](https://github.com/nestordemeure/pandas2numpy)

#### Deep learning:
- [Jax](https://github.com/google/jax)
- [Flax](https://github.com/google/flax)

#### Plotting:
- [Matplotlib](https://matplotlib.org/)
- [Plotly](https://plotly.com/python/)
- [Scikit-Plot](https://github.com/reiinakano/scikit-plot)

#### Feature importance
- [Scikit-Learn](https://scikit-learn.org/stable/)

## TODO

- redo bayesian optim ([optuna](https://optuna.org/)) ?
