# Feature extraction

This repository contains a Julia script to compute matrix features used to predict the performance and precision of an iterativ solver on a given problem.

## Features

### Basic

These features can be quickly computed but are expected to be information poor when it comes to the computation of the computing time and numerical error.

- number of rows
- number of non-zero
- number of non-zero in lower triangle
- number of non-zero in upper triangle
- maximum number of non zero per row
- largest element, in magnitude, along diagonal
- smallest non-zero element in magnitude along diagonal
- bandwith of lower triangle
- bandwith of upper triangle
- number of ones
- number of row that are strictly diagonaly dominant
- normalized sum of distances from diagonal of non-zero elements in lower triangle
- normalized sum of distances from diagonal of non-zero elements in upper triangle
- property showing how symmetric the matrix is
- property showing of skew symmetric the matrix is
- property showing how symmetric the non-zero pattern of the matrix is
- norm 1
- frobenius norm

### Advanced

These features require information on the spectrum of the matrix making them impractical for realistic use.
They are interesting in so far as we know for sure that they contain information pertinent to the computing time and numerical error.

- spectral radius
- second largest absolute eigenvalue
- non-zero smallest absolute eigenvalue
- sqrt(spectral radius / second largest eigenvalue)
- sqrt(peak) where peak is spectral radius / (trace - spectral radius)
- sqrt(condition number computed by matlab)
- s90
- smin
- s10

Note that a, very, coarse approximation of these features can be computed using the diagonal terms of the matrix.

