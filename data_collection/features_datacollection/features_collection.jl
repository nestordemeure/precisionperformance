# packages used
using LinearAlgebra
using SparseArrays

# download online
#import Pkg
#Pkg.add("MatrixMarket")
using MatrixMarket

#-----------------------------------------------------------------------
# Paths

featuresFolder = "/ccc/home/cont001/ocre/demeuren/tmp/matrices/csvFiles/features/"
#outFile = "./features.csv"
matrixFolder = "/ccc/home/cont001/ocre/demeuren/tmp/matrices/matrixMarketFiles/" 
#matrixFolder = "./matrices/"

#-----------------------------------------------------------------------
# functions

# returns a (min,max) pair where :
# - min is the minimum (in magnitude) non-zero element along the diagonal of mat 
# - max is the maximum (in magnitude) element along the diagonal of mat
function magnitudeMaximaDiag(mat)
  min_element = typemax(Float64)
  max_element = 0.
  for k = 1:size(mat, 1)
    x = mat[k,k]
    if abs(x) > abs(max_element)
      max_element = x
    end
    if (x != 0) && (abs(x) < abs(min_element))
      min_element = x
    end
  end
  if (max_element == 0.) || (min_element == typemax(Float64))
    (0., 0.)
  else
    (min_element, max_element)
  end
end

# return (nb_non_zero, nb_non_zero_lower, nb_non_zero_upper)
function count_non_zero(sparseMat)
  nb_non_zero = 0
  nb_non_zero_lower = 0
  nb_non_zero_upper = 0
  rows,cols, _ = findnz(sparseMat)
  for(r,c) in zip(rows,cols)
    nb_non_zero += 1
    if c >= r # upper triangle
      nb_non_zero_upper += 1
    end
    if r >= c # lower triangle
      nb_non_zero_lower += 1
    end
  end
  nb_non_zero, nb_non_zero_lower, nb_non_zero_upper
end 

# returns the maximum number of non-zero per row of the matrix
function maxNonZeroPerRow(sparseMat)
  # gets (sorted) row indices of the non-zero in the matrix
  rows, _, _ = findnz(sparseMat)
  sort!(rows)
  # finds longuest sequences of identical row indices
  max_sequence = 0
  current_element = 0
  current_sequence = 0
  for r in rows 
    if r == current_element
      current_sequence += 1
    else 
      max_sequence = max(current_sequence, max_sequence)
      current_element = r
      current_sequence = 1
    end
  end 
  max_sequence
end

# counts the number of rows that satisfy |M[r,r]| > sum_(c!=r)|M[r,c]|
function countDiagonaliDominantRows(sparseMat)
  nrows = size(sparseMat, 1)
  # compute sthe sum of absolute values for each row
  rowAbsSums = fill(0., nrows)
  rows, _, values = findnz(sparseMat)
  for (r,v) in zip(rows, values)
    rowAbsSums[r] += abs(v)
  end
  # counts the number of rows that are diagonaly dominant
  nbdiagonalyDominantRows = 0
  for (r,s) in enumerate(rowAbsSums)
    if s < 2.0*abs(sparseMat[r,r])
      nbdiagonalyDominantRows += 1
    end
  end
  nbdiagonalyDominantRows
end

# returns (lower bandwith, upper bandwith)
# https://en.wikipedia.org/wiki/Band_matrix#Bandwidth
function getBandwidths(sparseMat)
  upperBand = 0
  lowerBand = 0
  rows, cols, _ = findnz(sparseMat)
  for (r,c) in zip(rows, cols)
    if c > r # upper triangle
      upperBand = max(upperBand, c-r)
    elseif r > c # lower triangle
      lowerBand = max(lowerBand, r-c)
    end
  end
  (lowerBand, upperBand)
end

# computes the normalized sum of distances from diagonal of non-zero elements in lower/upper triangles
# returns (lowerSpread, upperSpread)
function getSpreads(sparseMat)
  upperSpread = 0
  lowerSpread = 0
  rows, cols, _ = findnz(sparseMat)
  for (r,c) in zip(rows, cols)
    if c > r # upper triangle
      upperSpread += c - r
    elseif r > c # lower triangle
      lowerSpread += r - c
    end
  end
  # normalize the spreads 
  nrows = size(sparseMat, 1)
  nbTriangularElements = ((nrows - 1)*nrows) / 2
  upperSpread /= nbTriangularElements
  lowerSpread /= nbTriangularElements
  (lowerSpread, upperSpread)
end

# returns tree score :
# - property showing how symmetric the matrix is
#   1 - nnz(A - A^t) / (nnz(A) - nnz(diag(A)))
#   1 if the matrix is symmetric (could be used to reduce computations if needed)
# - property showing of skew symmetric the matrix is 
#   1 - (nnz(A + A^t) - nnz(diag(A))) / (nnz(A) - nnz(diag(A)))
# - property showing how symmetric the non-zero patternof  matrix  is
#   1 - nnz(P - P^t) / (nnz(P) - nnz(diag(P)))
#   where P is one where A is not 0 and 0 otherwise
function getsymmetricScore(sparseMat)
  rows, cols, values = findnz(sparseMat)
  nbAssymetric = 0
  nbPatternAssymetric = 0
  nbSkewAssymetric = 0
  nbNonDiagonalNonZero = 0
  nbDiagonalNonZero = 0
  for (r,c,v) in zip(rows, cols, values)
    if r != c 
      nbNonDiagonalNonZero += 1
      if sparseMat[c,r] != v 
        nbAssymetric += 1
      end
      if iszero(sparseMat[c,r])
        nbPatternAssymetric += 1
      end
      if sparseMat[c,r] != -v 
        nbSkewAssymetric += 1
      end
    else 
      nbDiagonalNonZero += 1
    end
  end
  if nbNonDiagonalNonZero == 0
    symmetricScore = 1
    skewSymmetricScore = 1
    patternSymmetricScore = 1 
  else
    symmetricScore = 1 - nbAssymetric / nbNonDiagonalNonZero
    skewSymmetricScore = 1 - (nbSkewAssymetric - nbDiagonalNonZero) / nbNonDiagonalNonZero
    patternSymmetricScore = 1 - nbPatternAssymetric / nbNonDiagonalNonZero
  end
  symmetricScore, skewSymmetricScore, patternSymmetricScore
end

# aproximate various metrics deduced from eigenvalues
# TODO do the non-approximate computations
function computeApproximateEigenvalueMetrics(mat)
  # eigenvalues
  lambda_max = 0. # largest absolute eigenvalue
  lambda_max2 = 0. # second largest absolute eigenvalue
  lambda_min = typemax(Float64) # smalest non-zero absolute eigenvalue
  for k = 1:size(mat, 1)
    x = abs(mat[k,k])
    if x > lambda_max
      lambda_max2 = lambda_max
      lambda_max = x
    elseif x > lambda_max2
      lambda_max2 = x
    end
    if (x != 0) && (x < lambda_min)
      lambda_min = x
    end
  end
  trace_mat = tr(mat)
  # quantities of interest
  if (lambda_max == 0.) || (lambda_min == typemax(Float64)) # there are only 0 on the diagonal
    lambda_min = 0.
    max_ratio = 1.
    peak = 1.
    condition_number = 1.
  else
    max_ratio = if (lambda_max2 == 0) 1. else sqrt(lambda_max / lambda_max2) end # should be infinite
    peak = if (trace_mat - lambda_max == 0) 1. else lambda_max / (trace_mat - lambda_max) end # should be infinite
    condition_number = if (lambda_max / lambda_min == Inf) 1. else sqrt(lambda_max / lambda_min) end # should be infinite
  end
  lambda_max, lambda_max2, lambda_min, max_ratio, peak, condition_number
end

# aproximate various metrics deduced from singular values
# TODO do the non-approximate computations
function computeApproximateSingularvalueMetrics(mat)
  diag_mat = sqrt.(abs.(diag(mat))) 
  nb_elements = length(diag_mat)
  s_max = maximum(diag_mat)
  s_min = minimum(diag_mat)
  # portion of s > 0.9*(s_max - s_min) + s_min
  threshold90 = (9*s_max + s_min) / 10
  s90 = count(diag_mat .> threshold90) / nb_elements
  # portion of s <= 0.1*(s_max - s_min) + s_min
  threshold10 = (s_max + 9*s_min) / 10
  s10 = count(diag_mat .<= threshold10) / nb_elements
  s_mid = 1 - (s90 + s10)
  s90, s10, s_mid
end

# function, reinforced against exceptions, that checks wetehr a matrix is postiv definite
function check_is_positiv_definite(mat)
  try
    isposdef(mat)
  catch e
    print(" (unable to test for positiv-definite property)")
    missing
  end
end

#-----------------------------------------------------------------------
# features extraction

# names of all features (does not include matrix name)
# see "Data-driven Performance Modelingof Linear Solvers for Sparse Matrices" for a full description
featureNames = ["is_symmetric", "is_hermitian", "is_triangular_lower", "is_triangular_upper", "is_positiv_definite", "nb_rows", "nb_non_zero", "nb_non_zero_lower", "nb_non_zero_upper", "max_non_zero_per_row", "nb_one", "nb_diagonal_dominant_rows", "smallest_diagonal", "sign_smallest_diagonal", "largest_diagonal", "sign_largest_diagonal", "lower_bandwidth", "upper_bandwidth", "lowerSpread", "upperSpread", "symmetric_score", "skew_symmetric_score", "pattern_symmetric_score", "norm_1", "norm_frobenius", "approx_lambda_max", "approx_lambda_max2", "approx_lambda_min", "approx_max_ratio", "approx_peak", "approx_condition_number", "s90", "smid", "s10"]

# computes the features for the matrix
function computeFeatures(mat)
  print(", basic features")
  matrix_type = typeof(mat)
  is_symmetric = issymmetric(mat)
  is_hermitian = ishermitian(mat)
  is_triangular_lower = (!(is_symmetric || is_hermitian)) && istril(mat)
  is_triangular_upper = (!(is_symmetric || is_hermitian)) && istriu(mat)
  nb_rows = size(mat, 1)
  nb_non_zero,nb_non_zero_lower,nb_non_zero_upper = count_non_zero(mat)
  max_non_zero_per_row = maxNonZeroPerRow(mat)
  smallest_diagonal, largest_diagonal = magnitudeMaximaDiag(mat)
  sign_smallest_diagonal = smallest_diagonal > 0.
  sign_largest_diagonal = largest_diagonal > 0.
  lower_bandwidth, upper_bandwidth = getBandwidths(mat)
  nb_one = count(isone, mat)
  nb_diagonal_dominant_rows = countDiagonaliDominantRows(mat)
  lowerSpread, upperSpread = getSpreads(mat)
  symmetric_score,skew_symmetric_score,pattern_symmetric_score = getsymmetricScore(mat)
  norm_1 = norm(mat, 1)
  norm_frobenius = norm(mat)
  approx_lambda_max, approx_lambda_max2, approx_lambda_min, approx_max_ratio, approx_peak, approx_condition_number = computeApproximateEigenvalueMetrics(mat)
  s90, s10, s_mid = computeApproximateSingularvalueMetrics(mat)

  print(", advanced features")
  is_positiv_definite = is_hermitian && check_is_positiv_definite(mat)
  # makes the matrix dense to compute condition number (too slow/memory hungry)
  # search an implem of eigmax/eigmin for sparse matrices
  #condition_number = sqrt(cond(Array(mat), 2))

  Any[is_symmetric, is_hermitian, is_triangular_lower, is_triangular_upper, is_positiv_definite, 
  nb_rows, nb_non_zero, nb_non_zero_lower, nb_non_zero_upper, max_non_zero_per_row, nb_one, nb_diagonal_dominant_rows,
  smallest_diagonal, sign_smallest_diagonal, largest_diagonal, sign_largest_diagonal,
  lower_bandwidth, upper_bandwidth, lowerSpread, upperSpread,
  symmetric_score, skew_symmetric_score, pattern_symmetric_score, 
  norm_1, norm_frobenius,
  approx_lambda_max, approx_lambda_max2, approx_lambda_min, approx_max_ratio, approx_peak, approx_condition_number, s90, s10, s_mid]
end

# writes the features as a line in the file
function writeFeatures(outFile, matrixName, features)
  write(outFile, matrixName)
  for feature in features
    write(outFile, '\t')
    write(outFile, string(feature))
  end
  write(outFile, '\n')
  flush(outFile)
end

# gets the size of the file with the given name and in the given folder
function getMatrixSize(matrixFolder, fileName)
  matrixPath = matrixFolder * fileName
  filesize(matrixPath)
end

# process a single matrix
function processMatrix(fileName, outFile)
  matrixName = splitext(fileName)[1]
  matrixPath = matrixFolder * fileName
  print("   [importing")
  mat = MatrixMarket.mmread(matrixPath)
  print(' ', typeof(mat)) # useful for debugging purposes
  if size(mat, 1) != size(mat, 2)
    println(" MATRIX IS NOT SQUARE]")
  else
    features = computeFeatures(mat)
    print(", exportation")
    writeFeatures(outFile, matrixName, features)
    println(']')
  end
end

#-----------------------------------------------------------------------

println("Starting...")

fileNames = sort(readdir(matrixFolder), by=f -> getMatrixSize(matrixFolder,f), rev=false)
for i = 1:length(fileNames)
   fileName = fileNames[i]
   matrixName = splitext(fileName)[1]
   outFile = featuresFolder * matrixName * ".csv"
   outFile_temp = featuresFolder * matrixName * "_temp.csv"
   if !isfile(outFile)
      print(i, '/', length(fileNames), " matrix: ", matrixName)
      open(outFile_temp, "w") do file
         writeFeatures(file, "matrix", featureNames)
         processMatrix(fileName, file)
      end
      mv(outFile_temp, outFile)
   end
end

println("Done.")

