# Precision vs Performance

We want to build a model that takes (features) and returns (estimated residual, second per iter).
With that, we can trace a curve of all those quantities as a function of the time or number of iterations.

## Pipeline

### matrix

The matrices from the original paper come from the [SuiteSparse Matrix collection](https://sparse.tamu.edu/) (which is derived from UFL).

### Run solver on matrix

Solve `Ax = b` with `x = 0` and `b` a random vector such that its entries are between `0` and the infinite norm of `A`.

A solver is considered converging if the relativ residual (`norm2(r) / norm2(b)`) goes below `10^-9` or if the number of iterations exceeds `10^4`.

### Solvers and preconditionners

The solvers originaly tested are : GMRES, GMRESCONDNUM, GMRESR, CG, CGCONDNUM, CGS, BICGSTAB, TFQMR, FIXEDPT.

The preconditionner originaly tested are : NONE, JACOBI, NEUMANN, LS, DOMDECOMP, ILU, ILUT, IC, ICT, CHEBYSHEV, POINTRELAXATION, BLOCKRELAXATION, AMESOS, SORA, IHSS.

### Features

#### Basic

These features can be quickly computed but are expected to be information poor when it comes to the computation of the computing time and numerical error.

- number of rows
- number of non-zero
- number of non-zero in lower triangle
- number of non-zero in upper triangle
- maximum number of non zero per row
- largest element, in magnitude, along diagonal
- smallest non-zero element in magnitude along diagonal
- bandwith of lower triangle
- bandwith of upper triangle
- number of ones
- number of row that are strictly diagonaly dominant
- normalized sum of distances from diagonal of non-zero elements in lower triangle
- normalized sum of distances from diagonal of non-zero elements in upper triangle
- property showing how symmetric the matrix is
- property showing of skew symmetric the matrix is
- property showing how symmetric the non-zero pattern of the matrix is
- norm 1
- frobenius norm

#### Advanced

These features require information on the spectrum of the matrix making them impractical for realistic use.
They are interesting in so far as we know for sure that they contain information pertinent to the computing time and numerical error.

- spectral radius
- second largest absolute eigenvalue
- non-zero smallest absolute eigenvalue
- sqrt(spectral radius / second largest eigenvalue)
- sqrt(peak) where peak is spectral radius / (trace - spectral radius)
- sqrt(condition number computed by matlab)
- s90
- smin
- s10

