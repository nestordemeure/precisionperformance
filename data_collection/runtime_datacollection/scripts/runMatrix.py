import argparse
import subprocess
import os
import sys

#--------------------------------------------------------------------------------------------------
# PARSE INPUTS

# parse inputs
# see https://docs.python.org/fr/3/howto/argparse.html
parser = argparse.ArgumentParser()
parser.add_argument("-e", "--explicit", action="store_true", help="request explicit residuals") # false by default
# for distributed work
job = parser.add_argument_group("job")
job.add_argument("-i", "--jobId", help="id of this particular job, from 0 to jobNumber excluded", type=int, default=0)
job.add_argument("-n", "--jobNumber", help="number of jobs (copy of this script running in parallel)", type=int, default=1)
# output paths
paths = parser.add_argument_group("paths")
defaultProgramPath = "/ccc/home/cont001/ocre/demeuren/thesis/code/PrecisionPerformance/runtime_datacollection/cmake-build-debug/dataAccumulation.exe"
paths.add_argument("-p", "--programPath", help="path to find the executable to run the benchmarks", default=defaultProgramPath)
defaultFolderPath = "/ccc/home/cont001/ocre/demeuren/tmp/matrices"
paths.add_argument("-f", "--folderPath", help="path to the folder containing the 'matrixMarketFiles' subfolder and where the outputs will be stored", default=defaultFolderPath)

# parses all arguments
args = parser.parse_args()

# parallel id
jobId = args.jobId
jobNumber = args.jobNumber
if jobId >= jobNumber: raise ValueError("You set an Id >= to the number of task screduled!")
if jobNumber == 1: print("Starting single process")
else: print("Starting process ", jobId, "/", jobNumber)

# paths
programPath = args.programPath
folderPath = args.folderPath
if programPath == defaultProgramPath: print("(using default program paths)")
if not os.path.exists(programPath): raise ValueError("Invalid program path!")
if folderPath == defaultFolderPath: print("(using default matrix paths)")
if not os.path.exists(folderPath + "/matrixMarketFiles"): raise ValueError("Invalid folder path!")

# residual
getExplicitResiduals = args.explicit
if getExplicitResiduals: print("(with explicit residual computation)")

#--------------------------------------------------------------------------------------------------
# GET MATRICES

# takes a filename and removes its extension
def removeExtension(filename):
    return os.path.splitext(filename)[0]

# getting the name of all the matrices
matrixFolder = folderPath + "/matrixMarketFiles/"
matrixNames = [filename for filename in os.listdir(matrixFolder) if filename.endswith(".mtx")]
matrixNames.sort(key=lambda filename: os.path.getsize(matrixFolder + '/' + filename), reverse=False) # sorts from smallest to largets using file size as a proxy
matrixNames = list(map(removeExtension, matrixNames))

#--------------------------------------------------------------------------------------------------
# RUN BENCHMARKING

# insures all needed directories exist
csvFolderPath = folderPath + "/csvFiles"
if not os.path.exists(csvFolderPath): os.makedirs(csvFolderPath)
timeFolderPath = csvFolderPath + "/time"
if not os.path.exists(timeFolderPath): os.makedirs(timeFolderPath)
implicitFolderPath = csvFolderPath + "/implicit"
if not os.path.exists(implicitFolderPath): os.makedirs(implicitFolderPath)
explicitFolderPath = csvFolderPath + "/explicit"
if not os.path.exists(explicitFolderPath): os.makedirs(explicitFolderPath)

# running script on all matrices
matrixId = 0
for matrixName in matrixNames:
    matrixPath = folderPath + "/matrixMarketFiles/" + matrixName + ".mtx"
    # path to store outputs
    timePath = timeFolderPath + '/' + matrixName + ".csv"
    implicitResidualPath = implicitFolderPath + '/' + matrixName + ".csv"
    explicitResidualPath = explicitFolderPath + '/' + matrixName + ".csv"
    # path to temporary storage
    timePath_temp = timeFolderPath + '/' + matrixName + "_temp.csv"
    implicitResidualPath_temp = implicitFolderPath + '/' + matrixName + "_temp.csv"
    explicitResidualPath_temp = explicitFolderPath + '/' + matrixName + "_temp.csv"
    # run data collection and rename temporary files
    if (matrixId%jobNumber == jobId) and (not os.path.exists(timePath)):
        print("- matrix ", matrixName, '\t', matrixId, '/', len(matrixNames))
        if getExplicitResiduals:
            subprocess.call([programPath, matrixName, matrixPath, timePath_temp, implicitResidualPath_temp, explicitResidualPath_temp])
            os.rename(explicitResidualPath_temp, explicitResidualPath)
        else:
            subprocess.call([programPath, matrixName, matrixPath, timePath_temp, implicitResidualPath_temp])
        os.rename(timePath_temp, timePath)
        os.rename(implicitResidualPath_temp, implicitResidualPath)
    matrixId += 1



