
j'ai compilé Trilinos et tu peux commencer à lancer des runs en t'inspirant du fichier: 

`/ccc/home/cont001/ocre/chevalierc/expe/data_time_collect.sh`

Pour le lancer, tu peux procéder de la manière suivante :

```
cd $CCCSCRATCHDIR/runs
ccc_msub ~/expe/data_time_collect.sh
```

Tu devrais voir apparaître des fichiers .o et .e qui correspondent à la sortie standard et à celle d'erreur du job.

Il y a une install de Trilinos dans mon `WORKDIR` et aussi la compilation de ton executable.

Si tu veux recompiler :

```
module purge
module load gnu/8.3.0
module load mkl
module load cmake/3.13.3
cmake -DCMAKE_C_COMPILER=$(which gcc) -DCMAKE_CXX_COMPILER=$(which g++) SRC_DIR
```

`ccc_mpp` to get state of jobs

to get files back :

```
scp -r demeuren@inti.ocre.cea.fr:/ccc/scratch/cont001/ocre/demeuren/PrecisionPerformance/csvFiles /ccc/home/cont001/ocre/demeuren/tmp/matrices/csvFiles
```

