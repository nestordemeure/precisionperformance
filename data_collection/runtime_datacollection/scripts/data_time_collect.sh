#! /bin/bash
#MSUB -r data_time_collect
#MSUB -n 4
#MSUB -q sklb
#MSUB -x
#MSUB -T 72000
#MSUB -E'--cpu-freq=highm1'

set -x
COMMAND="python /ccc/home/cont001/ocre/demeuren/thesis/code/PrecisionPerformance/runMatrix.py"
COMPUTE_BINARY=/ccc/home/cont001/ocre/demeuren/thesis/code/PrecisionPerformance/dataAccumulation.exe
EXPE_DIR=/ccc/scratch/cont001/ocre/demeuren/PrecisionPerformance/

cd ${BRIDGE_MSUB_PWD}

rm app.conf
for i in $(seq 0 $((${q}-1))) ; do
    echo "1   ${COMMAND} --explicit --jobId ${i} --jobNumber ${SLURM_NTASKS} --programPath ${COMPUTE_BINARY} --folderPath ${EXPE_DIR}" >> app.conf;
done

ccc_mprun -f app.conf

