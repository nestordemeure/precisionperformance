# for more information on the interfaces used, see :
#https://matrixdepotjl.readthedocs.io/en/latest/interface.html#interfaces
#https://github.com/JuliaSparse/MatrixMarket.jl

#using Pkg
#Pkg.add("MatrixDepot")
#Pkg.add("MatrixMarket")
using MatrixDepot
using MatrixMarket
using SparseArrays

# builds an iterator with the names of all the matrices that fit a given pattern
# typical pattern : "*/*" for the collections and "**" for everything
function listAvailableMatrix(pattern)
    names = MatrixDepot.listnames(pattern) # markdown
    names = names.content[1].rows # gets vector of vector of names + noise
    names = Iterators.flatten(names) # turns it into simple vector
    names = Iterators.filter(name -> occursin('/',name), names) # keeps only legal names
    names
end

# download a matrix and stores it into the matrices folder using the matrix market format
function downloadMatrix(matrixName, signalEnd=true)
    matrix = matrixdepot(matrixName)
    fileName = replace(matrixName, '/' => '_') # insures that no character that could be interpreted as a folder subsists
    fileName = "./matrices/" * fileName * ".mtx"
    MatrixMarket.mmwrite(fileName, convert(SparseArrays.SparseMatrixCSC, matrix)) # the matrix is converted to a normalized format
    println("saved '", matrixName, "' to disk.")
end

# download all the matrix fitting a given pattern
# typical pattern : "*/*" for the colections and "**" for everything
function downloadMatrices(pattern, displayProgress=true)
    # stores matrices in cache to speed up download
    MatrixDepot.load(pattern)
    for matrixName in listAvailableMatrix(pattern)
        downloadMatrix(matrixName, displayProgress)
    end
end

# download all available matrices
downloadMatrices("**")
