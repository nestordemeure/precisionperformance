# My solvers

The solver managers in this file are copies of Trilinos original solver managers.

They have been renamed and the variable `sTest_` has been set to `protected` to make it accessible to child classes.