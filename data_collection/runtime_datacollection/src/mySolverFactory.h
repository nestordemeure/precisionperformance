#pragma once

// factory
#include <BelosSolverFactory.hpp>
#include <Ifpack2_Factory.hpp>

// solvers
#include "solvers/MyBiCGStabSolMgr.hpp"
#include "solvers/MyBlockCGSolMgr.hpp"
#include "solvers/MyBlockGmresSolMgr.hpp"
#include "solvers/MyFixedPointSolMgr.hpp"
#include "solvers/MyGCRODRSolMgr.hpp"
#include "solvers/MyLSQRSolMgr.hpp"
#include "solvers/MyMinresSolMgr.hpp"
#include "solvers/MyPCPGSolMgr.hpp"
#include "solvers/MyPseudoBlockCGSolMgr.hpp"
#include "solvers/MyPseudoBlockGmresSolMgr.hpp"
#include "solvers/MyPseudoBlockTFQMRSolMgr.hpp"
#include "solvers/MyRCGSolMgr.hpp"
#include "solvers/MyTFQMRSolMgr.hpp"

//----------------------------------------------------------------------------------------------------------------------
// MISSING OVERLOADS

// adds the `setDebugStatusTest` function to a solver manager that does not implement it
template<class ScalarType, class MV, class OP, class SOLVER_MANAGER>
class WithStatus : public SOLVER_MANAGER
{
private:
    Teuchos::RCP<Belos::StatusTest<ScalarType,MV,OP> > debugStatusTest_;

public:
    // insures that clone keeps producing the class and not its parent
    Teuchos::RCP<Belos::SolverManager<ScalarType, MV, OP> > clone() const override
    {
        return Teuchos::rcp(new WithStatus<ScalarType,MV,OP,SOLVER_MANAGER>);
    }

    // adds a user-defined StatusTest
    void setDebugStatusTest(const Teuchos::RCP<Belos::StatusTest<ScalarType,MV,OP> > &debugStatusTest) override
    {
        debugStatusTest_ = debugStatusTest;
    }

    // overload solve to inject debugStatusTest
    // NOTE: this requires that sTest_ is defined as 'public' or 'protected' in SOLVER_MANAGER (it is 'private' by default)
    Belos::ReturnType solve()
    {
        // Add debug status test
        if (nonnull(debugStatusTest_) )
        {
            if (nonnull(SOLVER_MANAGER::sTest_))
            {
                Teuchos::rcp_dynamic_cast<Belos::StatusTestCombo<ScalarType,MV,OP>>(SOLVER_MANAGER::sTest_)->addStatusTest(debugStatusTest_);
            }
            else
            {
                SOLVER_MANAGER::sTest_ = debugStatusTest_;
            }
        }

        // solve system
        return SOLVER_MANAGER::solve();
    }
};

//----------------------------------------------------------------------------------------------------------------------
// FACTORY

/*
 * Class that implements a solver factory over all the solvers available
 * use variants of teh solvers when needed in order to provide proper stopping conditions methods
 */
template<class SC, class MV, class OP> class MySolverFactory : public Belos::Impl::SolverFactoryParent<SC, MV, OP>
{
public:
    MySolverFactory()
    {
        // https://github.com/trilinos/Trilinos/blob/f230c3c3cf6465d203dd338424c03137a92d47d0/packages/belos/src/Belos_Details_registerSolverFactory.cpp
        #define BELOS_DEFINE_REGISTER_SOLVER_MANAGER(manager,name) Belos::Impl::registerSolverSubclassForTypes<manager<SC,MV,OP>, SC, MV, OP> (name);
        #define STATUS_DEFINE_REGISTER_SOLVER_MANAGER(manager,name) Belos::Impl::registerSolverSubclassForTypes< WithStatus<SC,MV,OP,manager<SC,MV,OP>> , SC, MV, OP> (name);

        // "HYBRID BLOCK GMRES" (GmresPolySolMgr) has no status test to overload/modify and is thus excluded from the list
        // plus it converges in 0 iterations which is highly suspicious
        // "LSQR" and "FIXED POINT" do not converge for typical problems

        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyBiCGStabSolMgr, "BICGSTAB")
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyBlockCGSolMgr, "BLOCK CG") // Block CG
        BELOS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyBlockGmresSolMgr, "BLOCK GMRES") // Flexible GMRES
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyGCRODRSolMgr, "GCRODR") // Recycling GMRES
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyMinresSolMgr, "MINRES") // MINRES
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyPCPGSolMgr, "PCPG") // CGPoly, Seed CG
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyPseudoBlockCGSolMgr, "PSEUDOBLOCK CG") // PseudoBlockCG, Pseudo Block CG
        BELOS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyPseudoBlockGmresSolMgr, "PSEUDOBLOCK GMRES") // GMRES, Pseudo Block GMRES, PseudoBlockGMRES, PseudoBlockGmres
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyPseudoBlockTFQMRSolMgr, "PSEUDOBLOCK TFQMR") // Pseudoblock TFQMR, Pseudo Block Transpose-Free QMR
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyRCGSolMgr, "RCG") // Recycling CG
        STATUS_DEFINE_REGISTER_SOLVER_MANAGER(Belos::MyTFQMRSolMgr, "TFQMR") // TFQMR, Transpose-Free QMR

        #undef BELOS_DEFINE_REGISTER_SOLVER_MANAGER
        #undef STATUS_DEFINE_REGISTER_SOLVER_MANAGER
    };

    static std::vector<std::string> solverList;
};

// generates a preconditioner of the given type
// see https://github.com/trilinos/Trilinos_tutorial/wiki/IfpackBelos2
// and: https://github.com/trilinos/Trilinos_tutorial/wiki/IfpackBelos2
// and: https://github.com/trilinos/Trilinos_tutorial/wiki/SimpleTpetraBelos
template<class MatrixType>
Teuchos::RCP<Ifpack2::Preconditioner<typename MatrixType::scalar_type,
typename MatrixType::local_ordinal_type,
typename MatrixType::global_ordinal_type,
typename MatrixType::node_type> >
createPreconditionner (const std::string& precType, const Teuchos::RCP<const MatrixType>& matrix)
{
    Ifpack2::Factory precondFactory;
    if ((precType == "Richardson") or (precType == "Jacobi") or (precType == "Gauss-Seidel") or (precType == "Symmetric Gauss-Seidel"))
    {
        // those reconditionners are all a type of RELAXATION that needs to be stipulated via a parameter
        auto precond = precondFactory.create("RELAXATION", matrix);
        Teuchos::ParameterList params;
        params.set("relaxation: type", precType);
        precond->setParameters(params);
        return precond;
    }
    else
    {
        return precondFactory.create(precType, matrix);
    }
}

//----------------------------------------------------------------------------------------------------------------------
// LISTS

// list of solvers recognized by the factory
// the CG family of solvers is only proven for SPD matrices but might still work anyway
std::vector<std::string> listSolvers = {"BICGSTAB", "BLOCK GMRES", "MINRES", "PSEUDOBLOCK GMRES", "GCRODR", "PSEUDOBLOCK TFQMR", "TFQMR", "BLOCK CG", "PSEUDOBLOCK CG", "PCPG", "RCG"};

// list of preconditionners recognized by the factory
// see https://docs.trilinos.org/dev/packages/ifpack2/doc/html/classIfpack2_1_1Factory.html
// "RBILUK" fails with RBILUK::initialize: Cannot cast to filtered matrix
// "CHEBYSHEV" is only valid for SPD matrices (thus we expect it will not be recommended for other types of matrix)
// "RELAXATION" is replaced by its possible kinds
std::vector<std::string> listPreconditionners = {"NONE", "ILUT", "RILUK", "DIAGONAL", "CHEBYSHEV", "Richardson", "Jacobi", "Gauss-Seidel", "Symmetric Gauss-Seidel"};

// returns a list of possible preconditioner sides as boolean
// true => right preconditioner
// false => left preconditionner
std::vector<bool> listPreconditionerSides(const std::string& solverName, const std::string& precondName)
{
    // no preconditioner => no side difference
    if(precondName == "NONE") return {true};
    // always fails with precond left
    if(solverName == "BICGSTAB") return {true};
    return {true, false};
}

// muelu
