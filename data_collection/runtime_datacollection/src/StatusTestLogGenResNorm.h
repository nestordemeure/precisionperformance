#pragma once

#include <BelosStatusTest.hpp>
#include <Teuchos_ScalarTraits.hpp>
#include <Teuchos_RCP.hpp>
#include <BelosMultiVecTraits.hpp>

/*
 * StatusTestLogGenResNorm but with additional parameters to:
 * - store explicit residual
 * - use an arbitrary norm
 */

namespace Belos
{
    template<class ScalarType, class MV, class OP>
    class StatusTestLogGenResNorm : public StatusTest<ScalarType, MV, OP>
    {

    public:
        //! The type of the magnitude (absolute value) of a ScalarType.
        typedef typename Teuchos::ScalarTraits<ScalarType>::magnitudeType MagnitudeType;

    private:
        //! @name Abbreviations for method implementations
        //@{
        typedef MultiVecTraits <ScalarType, MV> MVT;
        //@}

    public:

        //! @name Constructor/Destructor.
        //@{ 

        //! Constructor
        StatusTestLogGenResNorm(int maxIters, bool storeExplicitResidual=false, NormType TypeOfNorm=TwoNorm);

        //! Destructor
        virtual ~StatusTestLogGenResNorm()
        {};
        //@}

        //! @name Status methods
        //@{ 

        //! Check convergence status of the iterative solver: Unconverged, Converged, Failed.
        /*! This method checks to see if the convergence criteria are met using the current information from the 
          iterative solver.
        */
        StatusType checkStatus(Iteration<ScalarType, MV, OP> *iSolver);

        //! Return the result of the most recent CheckStatus call.
        StatusType getStatus() const
        {
            return (Undefined);
        }

        bool hasExplicitResiduals() const
        {
            return (storeExplicitResidual_);
        }

        //@}

        //! @name Reset methods
        //@{ 

        //! Resets the status test to the initial internal state.
        void reset();

        //! Sets the maximum number of iterations allowed so internal storage can be resized.
        void setMaxIters(int maxIters)
        {
            maxIters_ = maxIters;
            logResNorm_.reserve(maxIters_);
            if(storeExplicitResidual_)
            {
                logResNormExplicit_.reserve(maxIters_);
            }
        }

        //@}

        //! @name Accessor methods
        //@{ 

        //! Returns the maximum number of iterations set in the constructor.
        int getMaxIters() const
        { return (maxIters_); }

        //! Returns the current number of iterations from the most recent StatusTest call.
        int getNumIters() const
        { return (nIters_); }

        //! Returns the log of the implicit absolute residual norm from the iteration.
        const std::vector<typename Teuchos::ScalarTraits<ScalarType>::magnitudeType> &getLogResNorm(bool explicitResidual=false) const
        {
            if(explicitResidual)
            {
                assert(storeExplicitResidual_);
                return (logResNormExplicit_);
            }
            else
            {
                return (logResNorm_);
            }
        }

        //@}

        //! @name Print methods
        //@{ 

        //! Output formatted description of stopping test to output stream.
        void print(std::ostream &os, int indent = 0) const;

        //! Print message for each status specific to this stopping test.
        void printStatus(std::ostream &os, StatusType type) const;

        //@}

        /** \name Overridden from Teuchos::Describable */
        //@{

        /** \brief Method to return description of the debugging status test  */
        std::string description() const
        {
            std::ostringstream oss;
            oss << "Belos::StatusTestLogGenResNorm<>: [ " << getNumIters() << " / " << getMaxIters() << " ]";
            return oss.str();
        }
        //@} 

    private:

        //! @name Private data members.
        //@{ 
        //! Maximum number of iterations allowed
        int maxIters_;

        //! Current number of iterations
        int nIters_;

        //! Log of implicit absolute residual norm
        std::vector<typename Teuchos::ScalarTraits<ScalarType>::magnitudeType> logResNorm_;

        //! Log of explicit absolute residual norm
        std::vector<typename Teuchos::ScalarTraits<ScalarType>::magnitudeType> logResNormExplicit_;

        //! Type of residual to use (explicit or implicit)
        bool storeExplicitResidual_;

        //! Type of norm to use on residual (OneNorm, TwoNorm, or InfNorm).
        NormType resnormtype_;

        //! Most recent solution vector used by this status test.
        Teuchos::RCP<MV> curSoln_;
        //@}

    };

    template<class ScalarType, class MV, class OP>
    StatusTestLogGenResNorm<ScalarType, MV, OP>::StatusTestLogGenResNorm(int maxIters, bool storeExplicitResidual, NormType TypeOfNorm)
    {
        maxIters_ = std::max(1, maxIters);
        logResNorm_.reserve(maxIters_);
        nIters_ = 0;
        resnormtype_ = TypeOfNorm;
        storeExplicitResidual_ = storeExplicitResidual;
        if(storeExplicitResidual_)
        {
            logResNormExplicit_.reserve(maxIters_);
        }
    }

    template<class ScalarType, class MV, class OP>
    StatusType StatusTestLogGenResNorm<ScalarType, MV, OP>::checkStatus(Iteration<ScalarType, MV, OP> *iSolver)
    {
        const LinearProblem<ScalarType, MV, OP> &lp = iSolver->getProblem();
        int blkSize = lp.getLSIndex().size();
        int numRHS = MVT::GetNumberVecs(*(lp.getRHS()));
        int currIters = iSolver->getNumIters();

        // Check that this solve is a single-vector, single-block.
        if ((numRHS == 1) && (blkSize == 1) && (currIters != nIters_))
        {
            std::vector<MagnitudeType> tmp_resvector(1);
            Teuchos::RCP<const MV> residMV = iSolver->getNativeResiduals(&tmp_resvector);
            if (!residMV.is_null())
            {
                // We got a multivector back.  Compute the norms explicitly.
                MVT::MvNorm(*residMV, tmp_resvector, resnormtype_);
            }
            logResNorm_.push_back(tmp_resvector[0]);

            if (storeExplicitResidual_)
            {
                // Request the true residual for this block of right-hand sides.
                Teuchos::RCP<MV> cur_update = iSolver->getCurrentUpdate();
                curSoln_ = lp.updateSolution(cur_update);
                Teuchos::RCP<MV> cur_res = MVT::Clone(*curSoln_, MVT::GetNumberVecs(*curSoln_));
                lp.computeCurrResVec(&*cur_res, &*curSoln_);
                MVT::MvNorm(*cur_res, tmp_resvector, resnormtype_);
                logResNormExplicit_.push_back(tmp_resvector[0]);
            }

            nIters_ = currIters;
        }

        return Undefined;
    }

    template<class ScalarType, class MV, class OP>
    void StatusTestLogGenResNorm<ScalarType, MV, OP>::reset()
    {
        nIters_ = 0;
        logResNorm_.clear();
        logResNorm_.reserve(maxIters_);
        if(storeExplicitResidual_)
        {
            logResNormExplicit_.clear();
            logResNormExplicit_.reserve(maxIters_);
        }
        curSoln_ = Teuchos::null;
    }

    template<class ScalarType, class MV, class OP>
    void StatusTestLogGenResNorm<ScalarType, MV, OP>::print(std::ostream &os, int indent) const
    {
        for (int j = 0; j < indent; j++)
            os << ' ';
        printStatus(os, Undefined);
        os << "Logging Absolute Residual 2-Norm" << std::endl;
    }

    template<class ScalarType, class MV, class OP>
    void StatusTestLogGenResNorm<ScalarType, MV, OP>::printStatus(std::ostream &os, StatusType type) const
    {
        os << std::left << std::setw(13) << std::setfill('.');
        os << "**";
        os << std::left << std::setfill(' ');
        return;
    }

} // end Belos namespace
