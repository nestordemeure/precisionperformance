#include <iostream>
#include <fstream>
#include <chrono>

// Shaman
//#include <shaman.h>
//#include <shaman/helpers/shaman_trilinos.h>

// Tpetra matrix format
#include <Tpetra_Core.hpp>
#include <Tpetra_Vector.hpp>
#include <Tpetra_MultiVector.hpp>
#include <Tpetra_Operator.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <MatrixMarket_Tpetra.hpp>

// Belos solvers
#include <BelosTpetraAdapter.hpp>
#include <BelosLinearProblem.hpp>
#include "mySolverFactory.h"

// residual logging
//#include <BelosStatusTestLogResNorm.hpp>
#include "StatusTestLogGenResNorm.h"

// definitions to shorten code
using Tpetra::Details::DefaultTypes::global_ordinal_type;
using Tpetra::Details::DefaultTypes::local_ordinal_type;
using Node = Kokkos::Compat::KokkosDeviceWrapperNode<Kokkos::Serial>;
using scalarType = double;
using Vector = Tpetra::Vector<scalarType>;
using MultiVector = Tpetra::MultiVector<scalarType, local_ordinal_type, global_ordinal_type, Node>;
using Matrix = Tpetra::CrsMatrix<scalarType, local_ordinal_type, global_ordinal_type, Node>;
#define Pointeur Teuchos::RCP
#define make_pointeur Teuchos::rcp
using Comm = Teuchos::SerialComm<int>;
using Map = Tpetra::Map<int, int, Node>;
using Operator = Tpetra::Operator<scalarType, local_ordinal_type, global_ordinal_type, Node>; // Matrix
using ResidualLogger = Belos::StatusTestLogGenResNorm<scalarType,MultiVector,Operator>;

//----------------------------------------------------------------------------------------------------------------------
// PARAMETERS

// the paper uses 10000 iter but we observed that convergence is almost always done within 2000 iter (5000 would be safer but much slower and still just adding late convergers anyway)
const int maxIter = 2000;

// 1.0e-9 in paper but we want to get full curves for all iterations
const double errorTolerance = 0.;

//----------------------------------------------------------------------------------------------------------------------
// DATA

// type of output for a run
enum ResultType {Convergence, NonConvergence, Error};

// stores data obtained during a run
struct Data
{
    std::string matrix;
    std::string preconditioner;
    bool isRightPrecond;
    std::string solver;
    ResultType output;
    std::chrono::duration<double> elapsedSeconds;
    scalarType absoluteResidual;
    scalarType normB;
    unsigned int nbIter;
    std::vector<scalarType> implicitResiduals;
    std::vector<scalarType> explicitResiduals;

    Data(std::string matrixName, std::string precondName, bool isRightPrec, std::string solverName, ResultType outputType, const std::chrono::duration<double>& elapsedTime, scalarType absoluteResidualArg, scalarType normBArg, unsigned int nbIterArg, const ResidualLogger& residualLogger):
    matrix(std::move(matrixName)), preconditioner(std::move(precondName)), isRightPrecond(isRightPrec), solver(std::move(solverName)), output(outputType), elapsedSeconds(elapsedTime), absoluteResidual(absoluteResidualArg), normB(normBArg), nbIter(nbIterArg), implicitResiduals(), explicitResiduals()
    {
        implicitResiduals = residualLogger.getLogResNorm(false);
        if(residualLogger.hasExplicitResiduals())
        {
            explicitResiduals = residualLogger.getLogResNorm(true);
        }
    }

    // print historic of relative residual
    void printResiduals(std::ofstream& stream, bool printExplicitResiduals=false) const
    {
        const char separator = '\t';

        // header of the line,
        stream << matrix << separator << solver << separator << preconditioner << separator;
        if (isRightPrecond)
        {
            stream << "right" << separator;
        }
        else
        {
            stream << "left" << separator;
        }

        // historic of relativ residuals
        if(printExplicitResiduals)
        {
            for (auto r: explicitResiduals)
            {
                stream << (r/normB) << separator;
            }
        }
        else
        {
            for (auto r: implicitResiduals)
            {
                stream << (r/normB) << separator;
            }
        }

        stream << std::endl;
    }
};

// produces a tab separated representation :
// matrix   solver  preconditionner precondSide    status  time residual    nbIter
std::ostream& operator<<(std::ostream& stream, const Data& data)
{
    const char separator = '\t';

    stream << data.matrix << separator << data.solver << separator << data.preconditioner << separator;

    if (data.isRightPrecond)
    {
        stream << "right" << separator;
    }
    else
    {
        stream << "left" << separator;
    }

    switch(data.output)
    {
        case ResultType::Convergence:
        {
            stream << "CONVERGENCE" << separator;
            break;
        }
        case ResultType::NonConvergence:
        {
            stream << "NON-CONVERGENCE" << separator;
            break;
        }
        case ResultType::Error:
        {
            stream << "ERROR" << separator;
            break;
        }
    }

    stream << data.elapsedSeconds.count() << separator << (data.absoluteResidual/data.normB) << separator << data.nbIter;

    return stream;
}



//----------------------------------------------------------------------------------------------------------------------
// BUILDERS

// defines a linear problem of the form A*x = b
// with x = zero
// and b filled with random values between 0 and normInf(A)
std::pair<Pointeur<Vector>,Pointeur<Vector>> defineProblem(Pointeur<const Matrix> A)
{
    //scalarType normInfA = A->normInf();
    auto view = A->getLocalValuesView();
    scalarType normInfA = *std::max_element(view.data(), view.data() + view.size(), [](scalarType x, scalarType y){ return (fabs(x) < fabs(y)); });
    normInfA = fabs(normInfA);

    // zero initialized x vector
    Pointeur<const Map> domainMap = A->getDomainMap();
    Pointeur<Vector> x = make_pointeur(new Vector(domainMap, true));

    // b vector filled with random values between 0 and normInf(A)
    Pointeur<const Map> rangeMap = A->getRangeMap();
    Pointeur<Vector> b = make_pointeur(new Vector(rangeMap, false));
    b->randomize(); // in [-1;1]
    b->abs(*b);
    b->scale(normInfA);

    return {x, b};
}

// applies the right|left preconditionner of the given name on the problem
// WARNING purposefully takes the problem by value to copy it
void appliesPreconditioner(Belos::LinearProblem<scalarType, MultiVector, Operator>& problem,
                           Pointeur<const Matrix> A,
                           const std::string& precondName,
                           bool isrightPrec)
{
    if (precondName != "NONE") // != None
    {
        auto precond = createPreconditionner(precondName, A);
        precond->initialize();
        precond->compute();
        if (isrightPrec)
        {
            problem.setRightPrec(precond);
        }
        else
        {
            problem.setLeftPrec(precond);
        }
    }
}

// builds the solver corresponding to the given name and feeds it the problem
// the solver will run for a maximum of 10000 and with a convergence tolerance of 1e-9
Pointeur<Belos::SolverFactory<scalarType, MultiVector, Operator>::solver_base_type> defineSolver(Belos::LinearProblem<scalarType, MultiVector, Operator>& problem, const std::string& solverName)
{
    //Belos::TpetraSolverFactory<scalarType, MultiVector, Operator> solverFactory;
    MySolverFactory<scalarType, MultiVector, Operator> solverFactory;

    // solver parameters
    // see https://trilinos.org/docs/dev/packages/belos/doc/html/BlockCG_2BlockCGEpetraExFile_8cpp-example.html
    Teuchos::ParameterList solver_params;
    //solver_params.set("Verbosity", Belos::Errors + Belos::IterationDetails + Belos::FinalSummary);
    solver_params.set("Verbosity", 0);
    //solver_params.set("Output Frequency", 0);
    solver_params.set("Maximum Iterations", maxIter);
    solver_params.set("Convergence Tolerance", errorTolerance);

    // create solver
    // see https://docs.trilinos.org/dev/packages/belos/doc/html/classBelos_1_1SolverFactory.html#details
    //auto solver = make_pointeur(new Belos::BlockCGSolMgr<scalarType, MultiVector, Operator>(make_pointeur(&problem,false), make_pointeur(&solver_params,false)) );
    auto solver = solverFactory.create(solverName, make_pointeur(&solver_params,false));
    solver->setProblem(make_pointeur(&problem,false));

    return solver;
}

// runs the solver and displays informations on the run
std::pair<ResultType, unsigned int> runSolver(Pointeur<Belos::SolverFactory<scalarType, MultiVector, Operator>::solver_base_type> solver)
{
    //try
    {
        auto returnType = solver->solve();
        const int iterationNumber = solver->getNumIters();

        if (returnType == Belos::ReturnType::Converged)
        {
            return {ResultType::Convergence, iterationNumber};
        }
        else
        {
            return {ResultType::NonConvergence, iterationNumber};
        }
    }
    /*catch (...)
    {
        const int iterationNumber = solver->getNumIters();
        return {ResultType::Error, iterationNumber};
    }*/
}

//----------------------------------------------------------------------------------------------------------------------
// MAIN

int main(int argc, char *argv[])
{
    // inputs
    if (argc < 4)
    {
        throw std::invalid_argument("Error: please pass 'matrixName', 'matrixPath', 'outputPath', 'implicitResidualOutputPath' and, optionally, 'explicitResidualOutputPath' arguments to the program.");
    }
    std::string matrixName = argv[1];
    std::string matrixPath= argv[2];
    std::string outputPath = argv[3];
    std::string implicitResidualOutputPath = argv[4];

    // explicit residual
    bool computeExplicitResiduals = false;
    std::string explicitResidualOutputPath;
    if(argc > 5)
    {
        std::cout << "[with explicit residuals]" << std::endl;
        computeExplicitResiduals = true;
        explicitResidualOutputPath = argv[5];
    }

    // trilinos set-up
    // see : https://trilinos.org/docs/dev/packages/tpetra/doc/html/Tpetra_Lesson02.html
    Pointeur<const Comm> comm = make_pointeur(new Comm()); // using a serial com

    // parses the matrix
    Pointeur<const Matrix> A = Tpetra::MatrixMarket::Reader<Matrix>::readSparseFile(matrixPath, comm);

    //Pointeur<const Matrix> A = Tpetra::MatrixMarket::Reader<Matrix>::readSparseFile(matrixPath, comm);
    Pointeur<Vector> x, b; std::tie(x, b) = defineProblem(A);

    // get output file ready
    std::ofstream implicitResidualCsv(implicitResidualOutputPath);
    std::ofstream explicitResidualCsv(explicitResidualOutputPath);
    std::ofstream csv(outputPath);
    csv << "matrix\tsolver\tpreconditionner\tprecondSide\tstatus\ttime\tresidual\titerations" << std::endl;

    // runs the matrix with all combinaisons of solver/preconditionner/preconditionerSide
    for(auto& solverName : listSolvers)
    {
        for(auto& precondName: listPreconditionners)
        {
            for(bool isRightPrecond: listPreconditionerSides(solverName, precondName))
            {
                // makes a copy of the problem
                Pointeur<Vector> x0 = make_pointeur(new Vector(*x, Teuchos::Copy));
                Pointeur<Vector> b0 = make_pointeur(new Vector(*b, Teuchos::Copy));
                Belos::LinearProblem<scalarType, MultiVector, Operator> problem(A, x0, b0);
                problem.setProblem();

                // defines logger to monitor evolution of absolute residual
                // see https://docs.trilinos.org/dev/packages/belos/browser/doc/html/test__bl__gmres__complex__diag_8cpp_source.html
                auto residualLogger = ResidualLogger(maxIter, computeExplicitResiduals);

                try
                {
                    // runs solver
                    auto startTime = std::chrono::system_clock::now();
                    appliesPreconditioner(problem, A, precondName, isRightPrecond);
                    auto solver = defineSolver(problem, solverName);
                    solver->setDebugStatusTest( make_pointeur(&residualLogger, false) );
                    std::pair<ResultType,unsigned int> status_nbIter = runSolver(solver);
                    auto endTime = std::chrono::system_clock::now();

                    // computes residual
                    Pointeur<Vector> residualVector = make_pointeur(new Vector(*b, Teuchos::Copy));
                    A->apply(*x0, *residualVector, Teuchos::NO_TRANS, 1., -1.); // Ax - b
                    scalarType absoluteResidual  = residualVector->norm2();
                    scalarType normB = b->norm2();

                    // displays and stores result
                    Data result(matrixName, precondName, isRightPrecond, solverName, status_nbIter.first, endTime-startTime, absoluteResidual, normB, status_nbIter.second, residualLogger);
                    csv << result << std::endl;
                    result.printResiduals(implicitResidualCsv, false);
                    if(computeExplicitResiduals) result.printResiduals(explicitResidualCsv, true);
                    std::cout << result << std::endl;
                }
                catch (...)
                {
                    // we do not display on error
                    std::string side = "letf";
                    if (isRightPrecond) side = "right";
                    std::cerr << matrixName << '\t' << solverName << '\t' << precondName << '\t' << side << '\t' << "ERROR" << std::endl;
                }
            }
        }
    }

    std::cout << "Matrix '" << matrixName << "' Done." << std::endl;
    return 0;
}
