
library(dplyr)
library(softImpute)
library(factoextra)

residualThreshold <- 2

#------------------------------------------------------------------------------
# IMPORTATION

folder <- "../timeCollection/outputs"

# list of all file path
paths <- list.files(path=folder, pattern = "*.csv$", full.names = TRUE)

# builds a single data frame with all data
data <- data.frame()
for(pathi in paths)
{
   datai <- read.table(pathi, sep = '\t', header = TRUE)
   data <- rbind(data,datai)
}

# checks type of columns
#sapply(data,class)

#------------------------------------------------------------------------------
# CLEAN-UP

# fuse preconditionner and precondside
data$preconditionner <- paste(data$preconditionner, data$precondSide, sep='_')

# fuse NONE_right and NONE_left as they represent the same thing
data$preconditionner[(data$preconditionner == "NONE_left") | (data$preconditionner == "NONE_right")] <- 'NONE'
data$preconditionner <- as.factor(data$preconditionner)
# take the mean of identical NONE row (and remove precondSide by omiting it)
data <- data %>% group_by(matrix, solver, preconditionner) %>% summarise(time = mean(time), residual = mean(residual), status = first(status)) %>% ungroup()

# adds a solver_preconditionner column
data$solver_preconditionner <- paste(data$solver, data$preconditionner, sep='_') %>% as.factor()

# all value with a relativ residual over 10 are considered non converged
# TODO a lower threshold might make sense
levels(data$status) <- c(levels(data$status), 'RESIDUAL') # add one level to the factor
data$status[data$residual > residualThreshold] <- 'RESIDUAL'
#data$time[data$residual > residualThreshold] <- NA

# remove matrices that have zero known time
data <- data %>% group_by(matrix) %>% filter(!all(is.na(time))) %>% ungroup()
# redo factor since some levels have been removed
data$matrix <- factor(data$matrix)

# remove solver_preconditionners that have zero known time
data <- data %>% group_by(solver_preconditionner) %>% filter(!all(is.na(time))) %>% ungroup()
# redo factor since some levels have been removed
data$solver_preconditionner <- factor(data$solver_preconditionner)
data$solver <- factor(data$solver) 
data$preconditionner <- factor(data$preconditionner) 

# add logarithmic column
data$log_time <- log(data$time)

#------------------------------------------------------------------------------
# EXPORTATION

# exports data
outpath <- "./alldata.csv"
write.table(data, outpath, sep='\t', row.names = FALSE)

#------------------------------------------------------------------------------
# MATRIX

# turn dataframe into a matrix (filled with mostly NA)
coordinates <- list(levels(data$matrix), levels(data$solver_preconditionner))
mat <- matrix(nrow=nlevels(data$matrix), ncol=nlevels(data$solver_preconditionner), dimnames = coordinates)
mat[cbind(data$matrix, data$solver_preconditionner)] <- data$log_time

#------------------------------------------------------------------------------
# EMBEDDING

# computes the error of the reconstruction
computeError <- function(fits, mat)
{
   mat2 <- fits$u %*% diag(fits$d) %*% t(fits$v)
   error <- mat2 - mat
   #error <- (error*error) %>% mean(na.rm=TRUE) %>% sqrt() # root mean square error
   error <- error %>% abs() %>% mean(na.rm=TRUE) # mean error
   return (error)
}

# tries to compute embeddings using softimpute (a better modelisation would be available with pytorch)
fits <- softImpute(mat, rank.max=3) #, trace.it=TRUE)
computeError(fits, mat) 
# 0.22 with softimpute
# 0.14 with deep learning (+garantee on the generalization) but much slower/more complex to train

# reconstructs the matrix
mat2 <- fits$u %*% diag(fits$d) %*% t(fits$v)
row.names(mat2) <- row.names(mat)
colnames(mat2) <- colnames(mat)

# gets embeddings
u <- fits$u
row.names(u) <- row.names(mat)
v <- fits$v
row.names(v) <- colnames(mat)
embeddings <- rbind(u,v)
varType <- c(rep("matrix", times=nrow(u)), rep("solver", times=nrow(v))) %>% as.factor()

#------------------------------------------------------------------------------
# VISUALIZATION

res.pca <- prcomp(embeddings, center = FALSE, scale = TRUE)

X11()
fviz_pca_ind(res.pca, col.ind = varType)
