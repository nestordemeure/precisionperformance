#!/bin/sh

FOLDER=/ccc/home/cont001/ocre/demeuren/tmp/matrices/csvFiles

# features (with header)
echo "Merging features files..."
head -1 $(ls $FOLDER/features/*.csv| head -1) > $FOLDER/features.csv
tail -n +2 -q $FOLDER/features/*.csv >> $FOLDER/features.csv

# time (with header)
echo "Merging timing files..."
head -1 $(ls $FOLDER/time/*.csv| head -1) > $FOLDER/time.csv
tail -n +2 -q $FOLDER/time/*.csv >> $FOLDER/time.csv

# explicit residuals
echo "Merging explicit residual files..."
cat $FOLDER/explicit/*.csv > $FOLDER/explicit.csv

# implicit residuals
echo "Merging implicit residual files..."
cat $FOLDER/implicit/*.csv > $FOLDER/implicit.csv

echo "Done."

